---
title: "Data Storage"
quthor: "Min Wang and Kevin R. Coombes"
date: "2017-10-11"
---

This folder should, in principle, hold the cleaned data for this
project. The primary raw data source is the RNA sequencing data for 33
kinds of cancer; copies of these files can be found at `../raw`. These
were processed by an R script (`00-prepData.R`), a subsset of genes
for the IL4 pathway were slected, and the results were stored as
binary R data files in the `RDA` subdirectory. These files are **not**
under version control (because they are binary, large, and unchanging
(more or less)).

Other data sources included here are

+ `pathway genes.xls` contains lists of the Entrez Gene IDs for genes
  in several pathways.  Sheet 3 contains the gene list for the IL4
  Signaling Pathway. **We should not need this!**
+ `IL4-genes.txt` was manually exported from the previous file since
  there is no real reason to use Excel or to require an extra R
  package to manipulate it. **We should not need this!**
+ `GeneCards2EntrezGene.txt` contains a table mapping between GeneCard
  IDs and Entrez Gene IDs and symbols. **We should not need this!**
+ `IL4.gpml` was downloaded from
  [WikiPathways](http://www.wikipathways.org). It contains a
  description of the IL4 pathway in the Graphical Pathway Markup
  Language (GPML; see https://www.pathvisio.org/gpml/) used both by 
  WikiPathways and by [PathVisio](https://www.pathvisio.org/).
