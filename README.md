---
title: Using Thresher to Understand Transcription Factor Expression
author: Kevin R. Coombes
date: 2017-10-23
---

# Overview

Thresher is an R package that implements the "Thresher" clustering
algorithm, which combines principal component analysis, outlier
removal, and mixture modeling with von Mises-Fisher distributions to
cluster genes/proteins or patients/samples.

Our general hypothesis is that we can use Thresher to decompose a
biological pathway into reproducible, coherent, biologically
interpretable "pathway building blocks" (PBBs). 

In this study, we test this idea by applying Thresher to the
expression of a (nearly?) complete set of transcription factor
expression data taken from the broad compendium of cancer data
produced by The Cancer Genome Atlas (TCGA).


Acknowledgements
----------------

The initial file and directory structure of this project was developed
by a group of participants in the Reproducible Science Curriculum
Workshop, held at [NESCent] in December 2014. The structure is based
on, and heavily follows the one proposed by [Noble 2009], with a few
but small modifications. The template and all other content in the
[rr-init repository] is distributed without any warranty. 

[rr-init repository]: https://github.com/Reproducible-Science-Curriculum/rr-init
[NESCent]: http://nescent.org
[Noble 2009]: http://dx.doi.org/10.1371/journal.pcbi.1000424
