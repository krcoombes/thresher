#! perl -w

use strict;
use warnings;
use LWP::Simple;

my $base = "https://www.ncbi.nlm.nih.gov/UniGene/clust.cgi?ORG=Hs";
#my $eid = '8092';
#my $ug = "Hs.41683";

open(OUT, ">", "../results/tissue.tsv") or die "Unable to create 'tissue.tsv': $!\n";
print OUT "ENTREZID\tUNIGENE\tTISSUE\n";
open(UG, "<", "../results/ug.tsv") or die "Unable to open 'ug.tsv': $!\n";
my $ignore = <UG>;
while (my $record = <UG>) {
    chomp $record;
    my @R = split /\t/, $record;
    my $eid = $R[0];
    my $ug = $R[1];

    my $cid = $ug;
    $cid =~ s/^Hs\.//;
    my $url = "$base&CID=$cid";
    print STDERR "$url\n";
    ## next line makes an HTTP call to the the NCBI UniGene database
    my $content = get($url) or die "Couldn't get '$ug'!";
    my @lines = split /\n/, $content;

    foreach my $i (0..$#lines) {
	my $line = $lines[$i];
	next unless $line =~ /cDNA Source/;
	$line = $lines[1 + $i];
	if ($line =~ /TEXT>(.*?)<\/TD/) {
	    my @tissue = split /; /, $1;
	    foreach my $t (@tissue) {
		print OUT "$eid\t$ug\t$t\n";
	    }
	} else {
	    print STDERR "bad line\n";
	}
	last;
    }
    sleep 10;  # Be polite about these calls ot the server
#    last if $eid eq '8092';
}

close(UG);
close(OUT);


exit;
__END__
