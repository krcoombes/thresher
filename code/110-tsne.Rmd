---
title: "t-SNE and Transcription Factors"
author: "Kevin R. Coombes"
date: '`r Sys.Date()`'
output:
  html_document:
    highlight: kate
    theme: yeti
    toc: yes
---

```{r globopt}
knitr:::opts_chunk$set(fig.path = "../results/knit10figs/")
```

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Getting Started
First, we load some global information about where to find pieces of
this project. (This binary R data file was created by the script
`100prepData.R`, which also collects transcription factors from the
full TCGA RNA-Sequencing data set.)
```{r globals}
source("00-paths.R")
scratch <- paths$scratch
load(file.path(scratch, "globals.Rda"))
rm(allCancers, rawDataDir, rdaDir)
ls()
```
We also load the intermediate results from the first analysis,
performed in `101-studyTF.Rmd`.
```{r inter}
load(file.path(scratch, "101-intermediate.Rda"))
rm(allData, cancerMean, pcScore)
ls()
```

## Transcription Factor Info
We also load a list of the transcription factors, along with the clusters to which they belong.
```{r tfs}
bcs <- read.csv(file.path("..", "results", "geneClust.csv"))
dim(bcs)
```

## Megadata
Here we load the full RNA-Sequencing data set for all 33 TCGA cancer
types; this binary R data file was created by report `103-OddsNEvens`.
```{r mega}
load(file.path(scratch, "megadata.Rda"))
ls()
```

### Summary by Cancer Type

```{r tab}
tab <- table(allAnno$cancerType)
x <- rownames(tab)
y <- as.vector(as.matrix(tab))
daft <- data.frame(x[1:11], y[1:11], x[12:22], y[12:22], x[23:33], y[23:33])
colnames(daft) <- rep(c("Cancer Type", "N"), 3)
library(xtable)
xtab <- xtable(daft, align="r|lr|lr|lr|", label = "cancerType",
               caption="Number of samples per caption type.")
print(xtab, include.rownames=FALSE,caption.placement = "top", file="../doc/paper/ct-table.tex")
```

## Transcription Factor Expression
Now we extract just the transcription factor expression data.
```{r tfdata}
tfdata <- as.matrix(megadata[as.character(bcs$Hs.SYMBOL),])
brdata <- as.matrix(megadata[c("ERBB2", "ESR1", "PGR"),])
rm(megadata, unweightedScore)
gc()
```
# Nonlinear t-SNE Maps

First, we check to make sure that there are no duplicates in the data set.
```{r checkdups}
checkdups <- duplicated(apply(tfdata, 1, paste, collapse="|"))
any(checkdups)
rm(checkdups)
```

Next, we fit a t_SNE model to the data., focusing on the samples, which arise
from different cancer types.
```{r cancers, fig.cap="Samples, colored by cancer type.", fig.width=12, fig.height=12}
library(Rtsne)
set.seed(97531)
tic <- system.time(
  cancers <- Rtsne(t(tfdata), initial_dims = 30)
)
tic

colvec <- colorScheme[as.character(allAnno$cancerType)]
sym <- c(16, 2, 8)[as.numeric(allAnno$Cat)]
xcent <- tapply(cancers$Y[,1], list(allAnno$cancerType), median)
ycent <- tapply(cancers$Y[,2], list(allAnno$cancerType), median)

plot(cancers$Y, pch=sym, col=colvec, xlab="T1", ylab="T2", cex=0.8)
text(xcent, ycent, levels(allAnno$cancerType), col='black', cex=1.5)
dev.copy(pdf, file="tf-tsne.pdf", width=12, height=12)
dev.off()
```

```{r echo=FALSE,eval=FALSE}
isESCA <- allAnno$cancerType == "ESCA"
summary(isESCA)
vals <- -4 - 1/2*cancers$Y[,1] - cancers$Y[,2]
plot(cancers$Y[isESCA, ], col = c("blue", "red")[1+1*(vals[isESCA]>0)])
abline(-4, -1/2)
tfcat <- c("squamous", "adeno")[1+1*(vals[isESCA]>0)]
tfESCA <- data.frame(ID = colnames(tfdata)[isESCA], Type = tfcat)
save(tfESCA, file = "tfESCA.rda")
```

```{r nml, echo=FALSE, results='hide',fig.width=12,fig.height=12}
sym2 <- sym
sym2[sym2==16] <- 1
plot(cancers$Y, pch=sym2, col=colvec, xlab="T1", ylab="T2", cex=0.8)
text(xcent, ycent, levels(allAnno$cancerType), col='black', cex=1.5)
```

```{r echo=FALSE,eval=FALSE, results="hide"}
pdf(file="fig2-nml.pdf", width=20, height=20, pointsize=24,bg="white")
plot(cancers$Y, pch=sym2, col=colvec, xlab="T1", ylab="T2", cex=0.8)
text(xcent, ycent, levels(allAnno$cancerType), col='black', cex=1.5)
dev.off()
```

```{r nml-zoom, echo=FALSE,results='hide',fig.width=12,fig.height=12}
plot(cancers$Y, pch=sym, col=colvec, xlab="T1", ylab="T2", cex=1.2,
     xlim=c(0, 40), ylim=c(-5, 15))
text(xcent, ycent, levels(allAnno$cancerType), col='black', cex=1.7)
```

It is clear that most kinds of tumor can be reasonably well separated by the
expression patterns of transcription factors. There are.however, a number of
interesting feratures present in this plot. First, note that primary tumors
are plotted with a solid circle, metastases with a hollow triangle, and normal
samples with an asterisk. 

1. Colon cancer (COAD) and rectal cancer (READ) are essentially indisitnguishable.
   (See the top of the figure, right of center.)
2. However, normal samples of COAD or READ can be distinguished from tumors,
   but not from each other.
3. The two types of lung cancer (LUAD and LUSC, upper left of center) can mostly be
   distinguished, although there are a few samples that overlap the wrong group.
4. However, all the normal lung samples end up in the same place.
5. Even though one type of primary kidney cancer (KICH, bottom middle) is very
   unlike the other two types (KIRC and KIRP, to the far right), their normal
   samples end up in the same place.
6. There are clearly two very different subtypes of esophogeal cancer (ESCA). One
   clusters with the stomach cancers (STAD; top center) while the other clusters
   with the head-and-neck cancers (HNSC; center).
7. Most of the time, we can tell normal samples from primary tumors. In addition to
   the lung, kidney, and colorectal that we have already mentioned, we can also
   separate subclusters of normal samples for thyroid (THCA; far left), liver
   (LIHC, upper left), prostate (PRAD, far left),
   and breast (BRCA, right center).
8. Breast cancer is also interesting, in that there are clearly at least two subtypes
   of breast cancer. 


## t-SNE for Genes
We want to mimic what we did in our `Thresher` analysis. So, we start by performing
our own principal components analysis.
```{r spca}
library(ClassDiscovery)
spca <- SamplePCA(tfdata)
```

Next, we extract the principal components, each of which is a linear combination of
transcription factors.  We are interested in the directione that nthey point, so
correlation distance for the weight vectors is a reasonabl measure of how they are
related.
```{r}
sc <- spca@components
DD <- as.dist(1 - cor(t(tfdata)))
set.seed(13579)
tis2 <- Rtsne(DD)
```

In order to plot the clusters, we need a separate color map from the one that
distinguishes cancer types. Not surprisingly, we construct this color scheme
using the `Polychrome` package.
```{r fig.cap="t-SNE plot."}
library(Polychrome)
set.seed(9641)
Dark30 <- createPalette(30, c("#2A95E8", "#E5629C"), range = c(10, 60),
   M = 100000)
names(Dark30) <- colorNames(Dark30)
clucol <- Dark30[bcs$Cluster]
```
Now we are ready for the plot
```{r fig.width=12, fig.height=12, fig.cap="t-SNE plot of transcripotion factors based on correlation of weight vectors."}
plot(tis2$Y, pch=16, type='n', xlab="T1", ylab="T2")
text(tis2$Y[,1], tis2$Y[,2], bcs$Cluster, col=clucol)
```

# Understanding breast cancer
The following plot shows that the extra cluster of BRCA samples consists of triple negatives.
```{r, fig.cap="Breast samples."}
isBRCA <- allAnno$cancerType == "BRCA"
brca <- data.frame(allAnno[isBRCA,], cancers$Y[isBRCA,], t(brdata[,isBRCA]))
pairs(brca[c("X1", "ERBB2", "ESR1")])

```

# Appendix
This analysis was performed in the following directory.
```{r where}
getwd()
```
This analysis was performed using the following R packages.
```{r si}
sessionInfo()
```
