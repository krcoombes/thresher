---
title: "Using Thresher to Study IL4 Signaling in NTYPE Cancer Types"
author: "Min Wang, Zachary B. Abrams, and Kevin R. Coombes"
date: "DATE"
output:
  html_document:
    highlight: kate
    theme: yeti
    toc: true
---

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Getting Started
First, we load some global information about where to find pieces of
this project.
```{r globals}
source("00-paths.R")
load(file.path(paths$scratch, "globals.Rda"))
ls()
```
Next, we set up a location to store the results.
```{r resultDir}
resultDir <- "../results"
if (!file.exists(resultDir)) dir.create(resultDir)
dateDir <- file.path(resultDir, "DATE")
if (!file.exists(dateDir)) dir.create(dateDir)
outputDir <- file.path(dateDir, 
                       paste("NTYPE", "SCALE", "DATASET", 
                             "ACTION", "SPACE", sep="_"))
if (!file.exists(outputDir)) dir.create(outputDir)
```

# Merging Data Across Cancer Types
We load in the data for the genes in the IL4 pathway across different
cancer types from TCGA.  Note that these data were prepared using the
script `00prepData.R` by selecting the genes of interest out of the
full RNA-Sequencing data sets.  We have to decide whether to use all
of the cancer types or just those that contain sufficiently many
(currently defined as 200) patients. While loading the data, we also
create a vector to keep track of the cancer type of each sample.
```{r allData}
allData <- NULL
ptnum <- rep(NA, length(tumorlist))
names(ptnum) <- tumorlist
cancerType <- NULL
for (tumor in tumorlist) {
  fname <- file.path(rdaDir,
                     paste(tumor, "Rda", sep='.'))
  load(fname)
  pnum <- nrow(rnadata1t)
  # skip small data sets unless we said we want 'all'
  if (NTYPE < 33 & pnum < 200) next 
  allData <- rbind(allData, rnadata1t)
  ptnum[tumor] <- pnum
  cancerType <- c(cancerType, rep(tumor, pnum))
  rm(genename1, rnadata1t)
}
```

The next line may look odd because of templatization, but we want to
be able to run this analysis on either the linear-raw scale or onthe
log-transformed scale.
```{r which.scale}
if ("SCALE" == "log") {
   allData <- log2(1 + allData)
}
```

Now we count some things.
```{r counts}
ptnum
sum(ptnum)
```

The TCGA barcodes include a part that identifies the sample types.
Types 1--9 are actual tumors; types 10-19 are different kinds of
normals.  We start by extracting this information.
```{r st}
rn <- rownames(allData)
st <- substring(rn, 14,15)
allAnno <- data.frame(cancerType=cancerType, sampleType=st)
summary(allAnno)
```
We see that there are `r sum(st>9)` normal samples (from different
kinds of cancer). Now we use this information to separate the normal
samples from the tumor samples.
````{r normalData}
normalData <- allData[st == 11,]
normalAnno <- allAnno[st == 11,]
```

```{r tumorData}
tumorData <- allData[st != 11,]
tumorAnno <- allAnno[st != 11,]
```

We also want to run the same analysis separately on these three
different data sets. Here we figure out which version we actually want
to use. 
```{r}
thisData <- DATASETData
dim(thisData)
```

# Determining the PC Dimension
We have investigated various methods for determiningthe "true"
dimension of the space of principal components.
## Variants of Bartlett's Method
Here we use various forms of Bartlett's method. They all clearly give
the wrong answer. First, we apply them to the full data set.
```{r bart}
suppressMessages( library(nFactors) )
nBartlett(data.frame(scale(thisData)), nrow(thisData)) 
```

## The Broken-Stick Model
Another standard way to determine the PC dimension is with the
broken-stick model.
```{r setupBS, fig.cap="Scree plot using all data."}
suppressMessages( library(PCDimension) )
spca <- SamplePCA(t(scale(thisData)))
lam <- spca@variances[1:(ncol(thisData) - 1)] 
obj <- AuerGervini(spca)
bsDimension(lam) 
screeplot(spca)
```

# Automated Auer-Gervini Model
In this seciton, we apply our automated Auer-Gevini method.  The basic
Auer-Gervini method relies on visual inspection of a graph of a step
function arising from a Bayesian model.  Our extension uses various
algorithms to automate the procedure of deciding which steps are
"long". Here are the algorithms we will use:
```{r algs}
agCPM <- makeAgCpmFun("Exponential")
agfuns <- list(twice=agDimTwiceMean, specc=agDimSpectral,
               km=agDimKmeans, km3=agDimKmeans3, 
               tt=agDimTtest, tt2=agDimTtest2,
               cpt=agDimCPT, cpm=agCPM)
```
Here are the results using all data.
```{r fig.cap="Auer-Gervini step function using all data."}
compareAgDimMethods(obj, agfuns) 
plot(obj, agfuns)
```

# Thresher
Now we use the Thresher algorithm to remove outliers and cluster the
features. Both to reduce typing and to ensure consistency, we use the
following function.
```{r makeClusters}
library(Thresher)
markClusters <- function(dataset, agfun, cutoff) {
  thresh <- Thresher(dataset, method="auer.gervini", scale=TRUE, agfun=agfun)
  outliers <- colnames(dataset)[thresh@delta <= cutoff] 
  reap <- Reaper(thresh, useLoadings = "SPACE" == "PC", cutoff = cutoff)
  fit1 <- apply(reap@fit$P, 1, which.max)
  clust1 <- rbind(data.frame(RNA = colnames(dataset)[thresh@delta <= cutoff],
                             cluster = rep(0, sum(thresh@delta <= cutoff))),
                  data.frame(RNA = rownames(reap@fit$P), cluster=as.numeric(fit1)))
  clust1$RNA <- as.character(clust1$RNA)
  clust1 <- clust1[order(clust1[,1]), ]
  list(thresher = thresh, reaper = reap, clusters = clust1,
       outliers = outliers, fit1 = fit1)
}
```

We apply this function to all the data.
```{r alltm}
thisResult <- markClusters(thisData, ACTION, 0.2)
```

```{r}
thisResult$outliers
thisResult$thresher@pcdim
thisResult$reaper@pcdim
thisResult$reaper@nGroups
thisResult$reaper@bic
```

# Coloring the GPML Diagram
We are going to use the clusters found by Thresher/Reaper to color the
nodes in the IL4 Signaling Pathway diagram.

## Color Scheme
First, we have to make a set of colors using the `Polychrome` package.
```{r Light24}
library(Polychrome)
f <- "Light24.rda"
if (file.exists(f)) {
  load(f)
} else {
  set.seed(701138)
  Light24 <- createPalette(24, "#ff0000", range = c(55, 95), M = 100000)
  Light24 <- c("#ffffff", Light24)
  names(Light24) <- colorNames(Light24)
  color <- sub("#", "", Light24)
  save(Light24, color, file=f)
}
```
Here is a picture of the color set.
```{r fig.cap="Color set."}
swatch(Light24)
```

## GPML
We will use this function to add colors to an existing GPML pathway
diagram. 
```{r gpml}
library(XML)
colorGPML <- function(clusters, sourceGPML, colorset=color) {
  ## read uncolored gpml file
  gpmlTree <- xmlTreeParse(sourceGPML, useInternal=TRUE)
  ## loop over 'DataNode' items
  ind1 <- as.numeric(which(names(xmlRoot(gpmlTree))=="DataNode"))
  for (i in ind1) {
    ## get the gene ID from the 'TextLabel'
    genei <- xmlAttrs(xmlChildren(xmlRoot(gpmlTree))[[i]])[["TextLabel"]]
    genei <- gsub("[[:space:]]", "", genei)
    if (genei %in% clusters$RNA) {              # if we recognize it
      indi <- which(clusters$RNA == genei)      # find all occurences
      colori <- colorset[clusters[indi, 2] + 1] # and color them accordingly
      addAttributes(xmlChildren(xmlChildren(xmlRoot(gpmlTree))[[i]])$Graphics, 
                    "FillColor" = colori)
    }
  }
  gpmlTree
}
```
Now we apply this function to the full data set.
```{r all.gpml}
IL4.tm <- colorGPML(thisResult$clusters, file.path(dataDir, "IL4.gpml"), color)
saveXML(IL4.tm, file.path(outputDir, "Colored_IL4.gpml"))
```

# Scores

```{r}
thisData2 <- data.frame(type=DATASETAnno$cancerType, thisData)
```

Score with equal weights for genes in each cluster.
```{r}
score1.mat <- matrix(0, nrow(thisData), thisResult$reaper@nGroups)
score1.std.mat <- matrix(0, nrow(thisData), thisResult$reaper@nGroups)
for (i in 1:thisResult$reaper@nGroups) {
  clustinm <- sort(rownames(thisResult$reaper@fit$P)[thisResult$fit1 == i])
  score1.mat[, i] <- apply(thisData[, clustinm], 1, mean) # unscaled data
  score1.std.mat[, i] <- apply(scale(thisData[, clustinm]), 1, mean) # scaled data
}
```

```{r beans}
library(beanplot)
colist <- as.list(palette36.colors(36)[-(1:2)])
for (p in 1:thisResult$reaper@nGroups) {
  png(file=file.path(outputDir, paste("score1-unscaled-", p, ".png", sep='')), 
    pointsize=16, width=5*512, height=1.5*512)
  beanplot(score1.mat[, p] ~ DATASETAnno$cancerType, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
  dev.off()
}
```

# Appendix
This analysis was performed in the following directory.
```{r where}
getwd()
```
This analysis was performed using the following R pacakges.
```{r si}
sessionInfo()
```
