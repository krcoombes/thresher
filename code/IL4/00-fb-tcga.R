locn <- "athome"

if (!require("FirebrowseR")) {
  if (!require("devtools")) install.packages("devtools")
  library(devtools)
  if (!require("httr")) install.packages("httr")
  library(httr)
  if (locn == "osumc") {
    set_config(
      use_proxy(url="proxy.osumc.edu", port=8080)
    )
  }
  install_github("mariodeng/FirebrowseR")

  detach("package:httr")
  detach("package:devtools")
}

library(FirebrowseR)

# get the list of cohorts
library(RJSONIO)
fetch <- function(myurl) {
  stuff <- paste(readLines(myurl), collapse='')
  if (stuff=="") return(NULL)
  struct <- fromJSON(stuff)
  length(struct)
  names(struct)
  gunk <- struct[[1]]
  NR <- length(gunk)
  NC <- length(gunk[[1]])
  daft <- matrix("", nrow=NR, ncol=NC)
  for (i in 1:NR) {
    for (j in 1:NC) {
      x <- gunk[[i]][[j]]
      daft[i,j] <- ifelse(is.null(x), NA, x)
    }
  }
  colnames(daft) <- names(gunk[[1]])
  daft <- as.data.frame(daft)
  daft
}

f <- "cohorts.rda"
if (file.exists(f)) {
  load(f)
} else {
  myurl <- "http://firebrowse.org/api/v1/Metadata/Cohorts?format=json"
  cohorts <- fetch(myurl)
  rm(myurl)
  save(cohorts, file=f)
}
rm(f)


################################
# get significantly mutated genes
getmsg <- function(cohort) {
  parameters = list(format = "json", cohort = cohort, tool = "MutSig2CV", 
                    rank = "", gene = "", q="", page = 1, page_size = 2001,
                    sort_by = "cohort")
  myurl = FirebrowseR:::build.Query(parameters = parameters, invoker = "Analyses", 
                                    method = "Mutation/SMG")
  msg <- fetch(myurl)
  numcol <- c(1, 6:21)
  for(n in numcol) msg[,n] <- as.numeric(as.character(msg[,n]))
  msg
}

f <- "msg.rda"
if (file.exists(f)) {
  load(f)
} else {
  msgs <- list()
  for (coh in as.character(cohorts$cohort)) {
    cat(coh, "\n", file=stderr())
    msgs[[coh]] <- getmsg(coh)
  }
  rm(coh)
  save(msgs, file=f)
}
rm(f)

swee <- unlist(lapply(msgs, function(x) x$p))
hist(swee, breaks=234)
cue  <- unlist(lapply(msgs, function(x) sum(x$q < 0.1)))
barplot(cue)

siggenes  <- sort(unique(as.character(unlist(
  lapply(msgs, function(x) x$gene[x$q < 0.1])))))
length(siggenes)

rm(cue, swee)
################################
# get MAF data

getmaf <- function(cohort, gene) {
  parameters = list(format = "json", cohort = cohort, tool = "MutSig2CV", 
                    gene = gene, tcga_participant_barcode = "", 
                    column = "", page = 1, page_size = 2001, 
                    sort_by = "cohort")
  myurl = FirebrowseR:::build.Query(parameters = parameters, invoker = "Analyses", 
                                  method = "Mutation/MAF")
  maf <- fetch(myurl)
  maf
}

N <- length(siggenes)/30
f <- "muts.rda"
if (file.exists(f)) {
  load(f)
} else {
  temuts <- list()
  for (coh in names(msgs)) { # only use cohorts where we got a list of genes
    for (n in 1:N) {
      genes <- siggenes[(n-1)*30 + (1:30)]
      lbl <- paste(coh, n, sep=":")
      if (!is.null(temuts[[lbl]])) next
      cat(lbl, "\n", file=stderr())
      temuts[[lbl]] <- getmaf(coh, genes)
    }
  }
  muts <- do.call(rbind, temuts)
  save(muts, file=f)
  rm(coh, gene, lbl, temuts)
}
rm(f)

# all of "COADREAD" is redundant
cr <- muts$cohort=="COADREAD"
muts <- muts[!cr,]
# all of STES is redundant
cr <- muts$cohort=="STES"
muts <- muts[!cr,]
# all of KIPAN is redundant
cr <- muts$cohort=="KIPAN"
muts <- muts[!cr,]
# all of GBMLGG is redundant
cr <- muts$cohort=="GBMLGG"
muts <- muts[!cr,]

attach(muts)
dup <- duplicated(lbl <- paste(Hugo_Symbol, tcga_participant_barcode, sep=":"))
detach()
any(dup)
sum(dup)

main <- muts[!dup,]
extras <- muts[dup,]
attach(extras)
any( duplicated(elbl <- paste(Hugo_Symbol, tcga_participant_barcode, sep=":")) )
detach()
extras$Label <- factor(elbl)
summary(extras)

ridiculous <- muts[lbl=="MUC16:TCGA-FW-A3R5",]

save(main, file="main.rda")

npat <- length(levels(main$tcga_participant_barcode))
freq <- table(main$Hugo_Symbol)/npat
plot(sort(as.vector(freq)))

attach(muts)
vt <- as.character(Variant_Type)
mutcount <- tapply(vt, list(tcga_participant_barcode, Hugo_Symbol),
              length)
disease <- tapply(as.character(cohort), list(tcga_participant_barcode),
                  function(x) x[1])
detach()
mutcount[is.na(mutcount)] <- 0
mutated <- 1 * (mutcount > 0)
all(names(disease) == rownames(mutcount))
table(disease)
plot(factor(disease), apply(mutated, 1, sum), ylim=c(0, 450))

h <- hist(mutcount, breaks=seq(-0.5, 146.5))$counts
barplot(L <- log10(h + 0.1))

disease <- factor(disease)
save(mutated, mutcount, disease, file="mutcount.rda")

################################
load("cohorts.rda")
load("mutcount.rda")
library(ade4)
dl <- levels(disease)

temp <- c("00", "33", "66", "99", "CC", "FF")
f <- function(x, y) paste(x, y, sep='')
c216 <- outer(paste("#", temp, sep=''),
              outer(temp, temp, f), f)
mat <- matrix(1:36, ncol=6)
opar <- par(mfrow=c(2,3))
for (i in 1:6) {
  image(mat, col=c216[,,i])
}
par(opar)

greens  <- colorRampPalette(c("#00FFFF", "#009900"))(7) # 1-7
pinks   <- colorRampPalette(c("#FFCCCC", "#CC0000"))(6) # 8-13
blues   <- colorRampPalette(c("#CC00FF", "#0066AA"))(7) # 14-20
oranges <- colorRampPalette(c("#A42A2A", "#FFA500"))(5) # 21-25
grays   <- colorRampPalette(c("#CCCCCC", "#000000"))(5) # 26-30
colors <- c(greens, pinks, blues, oranges, grays)
colors <- colors[c(26,7,8,9,27,
                   21,1,14,16,3,
                   4,5,2,15,6,
                   17,18,10,28,29,
                   19,22,23,24,13,
                   20,30,11,12,25)]
barplot(1:30, col=colors)
names(colors) <- dl

nPerPt <- apply(mutated, 1, sum)
plot(nPerPt, pch=16, col=colors[disease])

plot(disease, nPerPt, col=colors, ylim=c(0,200),
     ylab="Number of Mutations Per Patient")

f <- "bicur.rda"
if (file.exists(f)) {
  load(f)
} else {
  freq <- apply(mutated, 2, mean)
  expected <- outer(freq, freq, "*")*nrow(mutated)

  bicur <- t(mutated) %*% mutated
  dbin <- dist.binary(t(mutated), method=1) # jaccard
  hc <- hclust(dbin, "ward.D2")
  
  save(freq, expected, bicur, dbin, hc, file=f)
}
rm(f)

table(mutated[,"EGFR"], mutated[,"KRAS"], disease)

which.max(expected - bicur)/2740
which.max((expected-bicur)[,136])
rownames(bicur)[136]
table(BRAF=mutated[,"BRAF"], TP53=mutated[,"TP53"], disease)

which.max((expected - bicur)/expected^2)/2740
rownames(bicur)[458] # GDF1
which.max(((expected - bicur)/expected^2)[,458])
expected["GDF1","IER3IP1"]
bicur["GDF1","IER3IP1"]
table(GDF1=mutated[,"GDF1"], IER3IP1=mutated[,"IER3IP1"], disease)

library(ClassDiscovery)
g6 <- cutree(hc, k=6)
names(g6) <- colnames(mutated)
plotColoredClusters(hc, lab=colnames(mutated), col=g6)

sort(colnames(mutated)[g6==5])

plot(factor(cutree(hc, k=7)), freq, ylim=c(0,0.11))

f <- function(g) {
  if(is.numeric(g)) g<- rownames(expected)[g]
#  x <- sqrt(expected)[g,]
  x <- (expected)[g,]
  y <-  bicur[g,]
#  modl <- lm(y~x)
  plot(x, y, main=g, xlab="Sqrt(Expected)", ylab="Observed")
#  abline(coef(modl))
#  print(coef(modl))
}
f("TP53")

i <- sample(rownames(bicur), 1); f(i); lines(1:100, 12*(1:100)^(0.5))

smoothScatter(as.vector(expected), as.vector(bicur),
              xlim=c(0,100), ylim=c(0,100), nrpoints=1000)
abline(0,1)


partners <- function(x) {
  y <- apply(mutated[x==1,], 1, sum)-1
  hist(y, breaks=seq(-0.5, 1873.5), plot=FALSE)$counts
}

distribs <- apply(mutated, 2, partners)
v <- which(apply(distribs, 1, function(x) sum(x>0)) > 0)

################################
if (FALSE) {
  # the following command fails if you enter page_size=1446 or larger
  bark <- Analyses.Mutation.SMG(format="csv", cohort="BRCA", page_size=1446)
  class(bark)
  dim(bark)
  rm(bark)

  parameters = list(format = "csv", cohort = "BRCA", tool = "MutSig2CV", 
                    rank = "", gene = "", q="", page = 1, page_size = 1500,
                    sort_by = "cohort")
  myurl = FirebrowseR:::build.Query(parameters = parameters, invoker = "Analyses", 
                                    method = "Mutation/SMG")
  raw <- readLines(myurl)
  r <- read.table(myurl, stringsAsFactors=FALSE, header=TRUE, sep=',')
  q <- read.csv(myurl)
}

###########################
# indep sims

fakepts <- sapply(1:8000, function(i) rbinom(length(freq), 1, freq))

plot(freq, apply(fakepts, 1, mean))
abline(0,1)

opar <- par(mfrow=c(2,1))
hist(nPerPt[nPerPt < 70], breaks=123)
hist(fakePP <- apply(fakepts, 2, sum), breaks=123)
par(opar)

