#########################################################################
### Get the full TCGA RNA sequencing data, and save it as a single thing

## Here is where to get the full (sort-of-raw) data
source("00-paths.R")
rawDataDir <- paths$raw
tcgaDir <- file.path(rawDataDir, "TCGA-RNASeq")
if (!file.exists(tcgaDir)) stop("Cannot find tcga directory,", tcgaDir)

### Here is the list of tumor types. The exclusions are for artififcal datasets
### that are the unions of other sets.
allCancers <- dir(tcgaDir)
dontUse <- c("COADREAD", "GBMLGG", "KIPAN", "STES")
tumorlist <- allCancers[!(allCancers %in% dontUse)]
if (length(tumorlist) != 33) stop("Wrong number of tumor types.")
rm(dontUse, allCancers)

###################################
### Here is the main loop

ptnum <- rep(NA, length(tumorlist))
names(ptnum) <- tumorlist
cancerType <- NULL
for (k in 1:length(tumorlist)) {
  ## read rna-seq data
  tumor <- tumorlist[k]
  print(tumor)
  rawfile <- file.path(tcgaDir,
                       tumor,
                       paste(tumorlist[k], ".txt", sep=''))
  ## need to do this in two pieces because the second row just
  ## repeats "normalized counts" over and over. This is the part
  ## that is most likely to need changing if the data source fiddles
  ## with the format.
  ##
  ## KRC: The read.table command needs 'sep="\t"', else
  ##      you get the column names wrong.
  rnahead <- read.table(rawfile, header=TRUE, sep="\t",
                        nrow=1, row.names=1)
  rnadata <- read.table(rawfile, header=FALSE, sep="\t",
                        skip=2, row.names=1)
  colnames(rnadata) <- colnames(rnahead)
  rm(rnahead)

  pnum <- ncol(rnadata)
  ptnum[tumor] <- pnum
  cancerType <- c(cancerType, rep(tumor, pnum))
  if (exists("allData"))  { # then we'll bind the new stuff to existing
    if (all(rownames(allData) == rownames(rnadata))) {
      allData <- cbind(allData, rnadata)
    } else {
      stop("Mismatched gene names")
    }
  } else {                  # otherwise, we just create what we need
    allData <- rnadata
  }
  rm(rnadata, rawfile)
  gc()
} # end for k in 1:length(tumorlist)
rm(k, pnum, tumor)

## Get the preferred genenames by splitting on vertical bar (|) ...
genenames <- unlist(lapply(rownames(allData), function(x)  {
  strsplit(x,'[|][[:space:]]*')[[1]][1] 
}))
gid <- unlist(lapply(rownames(allData), function(x)  {
  strsplit(x,'[|][[:space:]]*')[[1]][2] 
}))
qs <- genenames == "?"
genenames[qs] <- gid[qs]
# fix duplicates
w <- which(duplicated(genenames))
w2 <- which(genenames == genenames[w])
gid[w2]
genenames[w2][1] <- paste(genenames[w2][1], gid[w2][1], sep='.')
## ... and use them
rownames(allData) <- genenames
anno <- data.frame(Entrex=gid)
rownames(anno) <- genenames
# clean up workspace
rm(genenames, gid, qs, w, w2, tumorlist)
rm(rawDataDir, tcgaDir)

allData <- as.matrix(allData)
cancerType <- factor(cancerType)
save(allData, anno, cancerType, ptnum,
     file = file.path(paths$clean, "allData.Rda"))

