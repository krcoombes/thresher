#########################################################################
### Would be nice to have R access to PathVisio directly

if (FALSE) {
# if (!require("RPathVisio")) {
  source("https://bioconductor.org/biocLite.R")
  biocLite("XMLRPC", suppressUpdates=TRUE)
  biocLite("BridgeDbR", suppressUpdates=TRUE)
  ## need the proxy only if behind the OSUMC firewall. Edit the logical as appropriate
  if (FALSE) {
    library("httr")
    set_config( use_proxy(url="http://proxy.osumc.edu", port=8080) )
  }
  library(devtools)
  ## For some reason, this fails. I think it's a 32-bit vs 64-bit issue.
  ## Complains about being unable to load 'rJava', which is already installed!?
  install_github("PathVisio/RPathVisio", subdir="RPathVisio")
}

#########################################################################
### load the libraries

if (FALSE) { # KRC: commented out while I figure out which ones we actually use
  library(PCDimension)
  library(nFactors)
  library(Thresher)
  library(ClassDiscovery)
  library(RColorBrewer)
  library(colorspace)
  library(changepoint)
  library(cpm)
  library(movMF)
  library(ade4)
  library(xlsx)
  library(beanplot)
}

#dir <- getwd()
#rdir <- dirname(getwd())
### rdir <- "C:/Users/wang.1807/Documents"

### load global information for the project
load("globals.Rda")

### set up a place to store results
resultDir <- "../results"
if (!file.exists(resultDir)) dir.create(resultDir)
temp <- file.path(resultDir, "33Tumors")
if (!file.exists(temp)) dir.create(temp)
outputDir <- file.path(temp, "2017-10-16")
if (!file.exists(outputDir)) dir.create(outputDir)
for (k in 1:length(tumorlist)) {
  sdir <- file.path(outputDir, tumorlist[k])
  if (!file.exists(sdir)) dir.create(sdir)
}
rm(k, sdir, temp)

#############################################################################
### Merged Data

mergedat <- NULL
ptnum <- rep(NA, length(tumorlist))
names(ptnum) <- tumorlist
for (tumor in tumorlist) {
  fname <- file.path(rdaDir,
                     paste(tumor, "Rda", sep='.'))
  load(fname)
  mergedat <- rbind(mergedat, rnadata1t)
  ptnum[tumor] <- nrow(rnadata1t)
  rm(genename1, rnadata1t)
}

ptnum
sum(ptnum)
# 10446
cumsum(ptnum)

cancerType <- rep(tumorlist, times=ptnum)

### sample types
rn <- rownames(mergedat)
st <- substring(rn, 14,15)
table(st)

anno <- data.frame(cancerType=cancerType, sampleType=st)
summary(anno)

normalData <- mergedat[st == 11,]
normalAnno <- anno[st == 11,]

tumorData <- mergedat[st != 11,]
tumorAnno <- anno[st != 11,]

#############################################################################
### Analysis of IL4 Signaling Pathway on Merged Data
## analyze the dimensionality of the rna-seq data

##################
### Bartlett and variants
library(nFactors)
nBartlett(data.frame(scale(mergedat)), nrow(mergedat)) 
# bartlett anderson   lawley 
#       53       53       53

nBartlett(data.frame(scale(tumorData)), nrow(tumorData)) 
nBartlett(data.frame(scale(normalData)), nrow(normalData)) 

##################
### Broken-stick
library(PCDimension)
agCPM <- makeAgCpmFun("Exponential")

agfuns <- list(twice=agDimTwiceMean, specc=agDimSpectral,
               km=agDimKmeans, km3=agDimKmeans3, 
               tt=agDimTtest, tt2=agDimTtest2,
               cpt=agDimCPT, cpm=agCPM)

spca.all <- SamplePCA(t(scale(mergedat)))
lam.all <- spca.all@variances[1:(ncol(mergedat)-1)] 
obj.all <- AuerGervini(spca.all)
bsDimension(lam.all) 
# 3
screeplot(spca.all)

spca.tumor <- SamplePCA(t(scale(tumorData)))
lam.tumor <- spca.tumor@variances[1:(ncol(mergedat)-1)] 
obj.tumor <- AuerGervini(spca.tumor)
bsDimension(lam.tumor) 
# 2
screeplot(spca.tumor)

spca.normal <- SamplePCA(t(scale(mergedat)))
lam.normal <- spca.normal@variances[1:(ncol(mergedat)-1)] 
obj.normal <- AuerGervini(spca.normal)
bsDimension(lam.normal) 
# 3
screeplot(spca.normal)

##################
### randomication-based (may skip because it is so slow)

rndLambdaF(scale(mergedat))
# rndLambda      rndF 
#         9        53

##################
### Auer-Gervini

compareAgDimMethods(obj.all, agfuns) 
# twice specc    km   km3    tt   tt2   cpt   cpm 
#     1     1     1     1    51     1     1     6
plot(obj.all, agfuns)

compareAgDimMethods(obj.tumor, agfuns) 
# twice specc    km   km3    tt   tt2   cpt   cpm 
#     1     1     1     1    51     1     1     6
plot(obj.tumor, agfuns)

compareAgDimMethods(obj.normal, agfuns) 
# twice specc    km   km3    tt   tt2   cpt   cpm 
#     1     1     1     1    51     1     1     6
plot(obj.normal, agfuns)


##################
### Use Thresher to remove outliers and cluster the features

library(Thresher)
markClusters <- function(dataset, agfun, cutoff) {
  thresh <- Thresher(dataset, method="auer.gervini", scale=TRUE, agfun=agfun)
  outliers <- colnames(mergedat)[thresh@delta <= cutoff] 
  reap <- Reaper(thresh, useLoadings=TRUE, cutoff=cutoff)
  fit1 <- apply(reap@fit$P, 1, which.max)
  clust1 <- rbind(data.frame(RNA = colnames(dataset)[thresh@delta <= cutoff],
                             cluster = rep(0, sum(thresh@delta <= cutoff))),
                  data.frame(RNA = rownames(reap@fit$P), cluster=as.numeric(fit1)))
  clust1$RNA <- as.character(clust1$RNA)
  clust1 <- clust1[order(clust1[,1]), ]
  list(thresher = thresh, reaper = reap, clusters = clust1, outliers  = outliers)
}

all.cpm <- markClusters(mergedat, agCPM, 0.2)

all.cpm$outliers
all.cpm$thresher@pcdim
all.cpm$reaper@pcdim
all.cpm$reaper@nGroups
all.cpm$reaper@bic

plot(all.cpm$thresher)
plot(all.cpm$reaper)

for (i in 1:all.cpm$reaper@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(all.cpm$reaper@fit$P)[all.cpm$reaper@fit==i]))
}


write.table(clust1, "Results\\Merge_IL4_clusters(CPM).csv", sep=",", col.names=NA, 
     row.names=TRUE)

save.image("merge.Rda")


##############################################################################
### build gpml diagrams for the clusters

# load packages
library("XML")

## set up colors
library(Polychrome)
set.seed(701138)
Light24 <- createPalette(24, "#ff0000", range = c(55, 95), M = 100000)
Light24 <- c("#ffffff", Light24)
names(Light24) <- colorNames(Light24)
swatch(Light24)
color <- sub("#", "", Light24)


colorGPML <- function(clusters, sourceGPML, colorset=color) {
  ## read uncolored gpml file
  gpmlTree <- xmlTreeParse(sourceGPML, useInternal=TRUE)
  ## loop over 'DataNode' items
  ind1 <- as.numeric(which(names(xmlRoot(gpmlTree))=="DataNode"))
  for (i in ind1) {
    ## get the gene ID from the 'TextLabel'
    genei <- xmlAttrs(xmlChildren(xmlRoot(gpmlTree))[[i]])[["TextLabel"]]
    genei <- gsub("[[:space:]]", "", genei)
    if (genei %in% clusters$RNA) {              # if we recognize it
      indi <- which(clusters$RNA == genei)      # find all occurences
      colori <- colorset[clusters[indi, 2] + 1] # and color them accordingly
      addAttributes(xmlChildren(xmlChildren(xmlRoot(gpmlTree))[[i]])$Graphics, 
                    "FillColor" = colori)
    }
  }
  gpmlTree
}

IL4 <- colorGPML(all.cpm$clusters, file.path(dataDir, "IL4.gpml"), color)
saveXML(IL4, file.path(outputDir, "Merge_IL4_CPM.gpml"))

############################################################################################
### Repeat using twice-mean

all.twicemean <- markClusters(mergedat, agDimTwiceMean, 0.2)

all.twicemean$outliers
all.twicemean$thresher@pcdim
all.twicemean$reaper@pcdim
all.twicemean$reaper@nGroups
all.twicemean$reaper@bic

plot(all.twicemean$thresher)
plot(all.twicemean$reaper)

for (i in 1:all.twicemean$reaper@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(all.twicemean$reaper@fit$P)[fit1==i]))
}

IL4.tm <- colorGPML(all.twicemean$clusters, file.path(dataDir, "IL4.gpml"), color)
saveXML(IL4.tm, file.path(outputDir, "Merge_IL4_TwiceMean.gpml"))

############################################################################################
### Principal component analysis on merged data

library(digest)
library(devtools)
install_github("ggbiplot", "vqv") 
library(ggbiplot)

mergedat2 <- mergedat
mergedat2 <- cbind(mergedat2, type=rep(0, nrow(mergedat2)))
tcumsum <- cumsum(ptnum)
tcumsum <- c(0, tcumsum)
mergedat2 <- data.frame(mergedat2)

for (i in 1:length(ptnum)) {
  mergedat2$type[(tcumsum[i]+1):tcumsum[i+1]] <- tumorlist2[i]
}

merge.pca <- prcomp(mergedat2[,-ncol(mergedat2)], center=TRUE, scale=TRUE) 
plot(merge.pca, type = "l")
g <- ggbiplot(merge.pca, obs.scale=1, var.scale=1, 
              groups=mergedat2$type, ellipse=TRUE, 
              circle=TRUE)
g <- g + scale_color_discrete(name='')
g <- g + theme(legend.direction='horizontal', 
               legend.position='top')
print(g)

for (i in 1:(length(tumorda2)-1)) {
  load(tumorda2[[i]])
  dat1 <- rnadata1t
  dat1 <- data.frame(dat1)
  dat1$type <- tumorlist2[i]
  for (j in (i+1):length(tumorda2)) {
    load(tumorda2[[j]])
    dat2 <- rnadata1t
    dat2 <- data.frame(dat2)
    dat2$type <- tumorlist2[j]
    merge2dat <- rbind(dat1, dat2)

    merge2.pca <- prcomp(merge2dat[,-ncol(merge2dat)], center=TRUE, scale=TRUE) 
    g <- ggbiplot(merge2.pca, obs.scale=1, var.scale=1, groups=merge2dat$type, 
              ellipse=TRUE, circle=TRUE)
    g <- g + scale_color_discrete(name='')
    g <- g + theme(legend.direction='horizontal', 
               legend.position='top')
    png(file=paste("Fig/", tumorlist2[i], "_", tumorlist2[j], "_PCA.png", sep=''))
    print(g)
    dev.off()
  }
}

save.image("merge.rda")


## investigate the projections of sample points in pooled data

library(Polychrome)
colv <- createPalette(33, seed=c("#aaaaaa", "#FA8E0D", "#682A93"), range=c(30, 70))
colcol <- rep(0, nrow(mergedat2))
for (i in 1:length(ptnum)) {
  colcol[(tcumsum[i]+1):tcumsum[i+1]] <- colv[i]
}

fit <- prcomp(mergedat2[,-ncol(mergedat2)], center=TRUE, scale=TRUE)

# projection of samples on PC space

plot(fit$x[,1], fit$x[,2], xlab="PC1", ylab="PC2", cex=0.5, type="p", pch=19,
    col=as.numeric(as.factor(mergedat2[,ncol(mergedat2)])))

for (p in 1:thresh1a@pcdim) {
  png(file=paste("PCA/PC", p, ".png", sep=''), pointsize = 18, width=2*512, 
    height=1.5*512)
  plot(fit$x[,p], xlab="Sample index", ylab=paste("PC ",p, sep=''), cex=0.5, 
    type="p", pch=19, col=colcol)
  dev.off()
}

hist(fit$x[,1], breaks=100)

for (p1 in 1:(thresh1a@pcdim-1)) {
  for (p2 in (p1+1):thresh1a@pcdim) {
    png(file=paste("PCA/PC", p1, "&", p2, ".png", sep=''), pointsize=18, width=2*512, 
      height=1.5*512)
    plot(fit$x[,p1], fit$x[,p2], xlab=paste("PC ",p1, sep=''), ylab=paste("PC ",p2, sep=''), 
      cex=0.5, type="p", pch=19, col=colcol)
    dev.off()
  }
}

plot(0,xaxt='n',yaxt='n',bty='n',pch='',ylab='',xlab='')
legend("center", tumorlist2, col=colv, cex=1.2, pch=19)

mat <- matrix(c(1,2), 1, 2, byrow=TRUE)
layout(mat, c(2.5), c(8,0.4))
par(mar=c(5, 5, 5, 0.5))
plot(fit$x[,p], xlab="Sample index", ylab=paste("PC ",p, sep=''), cex=0.5, 
    type="p", pch=19, col=colcol)
par(mar=c(2, 0.1, 2, 0.5))
plot(0,xaxt='n',yaxt='n',bty='n',pch='',ylab='',xlab='')
legend("center", tumorlist2, col=colv, cex=0.75, pch=19)

mat <- matrix(c(1,2), 1, 2, byrow=TRUE)
for (p in 1:thresh1a@pcdim) {
  png(file=paste("PCA/PC", p, ".png", sep=''), pointsize=18, width=3*512, 
    height=1.5*512)
  layout(mat, c(2.5), c(8,0.4))
  par(mar=c(5, 5, 5, 0.5))
  plot(fit$x[,p], xlab="Sample index", ylab=paste("PC ",p, sep=''), cex=0.5, 
    type="p", pch=19, col=colcol)
  par(mar=c(2, 0.1, 2, 0.5))
  plot(0,xaxt='n',yaxt='n',bty='n',pch='',ylab='',xlab='')
  legend("center", tumorlist2, col=colv, cex=0.9, pch=19)
  dev.off()
}

for (p1 in 1:(thresh1a@pcdim-1)) {
  for (p2 in (p1+1):thresh1a@pcdim) {
    png(file=paste("PCA/PC", p1, "&", p2, ".png", sep=''), pointsize=18, width=2*512, 
      height=1.5*512)
    layout(mat, c(2.5), c(8, 0.4))
    par(mar=c(5, 5, 5, 0.5))
    plot(fit$x[, p1], fit$x[, p2], xlab=paste("PC ", p1, sep=''), ylab=paste("PC ", p2, sep=''), 
      cex=0.5, type="p", pch=19, col=colcol)
    par(mar=c(2, 0.1, 2, 0.5))
    plot(0, xaxt='n', yaxt='n', bty='n', pch='', ylab='', xlab='')
    legend("center", tumorlist2, col=colv, cex=0.9, pch=19)
    dev.off()
  }
}


## look at two "scores" on distinguishing the tumors

for (i in 1:reaper1a@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(reaper1a@fit$P)[fit1==i]))
}

# score with equal weights for genes in each cluster
score1.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score1.std.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
for (i in 1:reaper1a@nGroups) {
  clustinm <- sort(rownames(reaper1a@fit$P)[fit1 == i])
  score1.mat[, i] <- apply(mergedat[, clustinm], 1, mean) # unscaled data
  score1.std.mat[, i] <- apply(scale(mergedat[, clustinm]), 1, mean) # scaled data
}

colist <- list()
for (s in 1:length(colv)) {
  colist[[s]] <- colv[s]
}

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCA/Scores/score1 (no scale) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=1.5*512)
  beanplot(score1.mat[, p] ~ mergedat2$type, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
  dev.off()
}

png(file=paste("PCA/Scores/score1 (no scale) all clusters.png", sep=''), 
  pointsize=16, width=5*512, height=3.5*512)
par(mfrow=c(6,1))
par(mar=c(2.5, 4, 1.5, 2))
for (p in 1:reaper1a@nGroups) {
  beanplot(score1.mat[, p] ~ mergedat2$type, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
}
dev.off()

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCA/Scores/score1 (scaled) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=1.5*512)
  beanplot(score1.std.mat[,p] ~ mergedat2$type, xlab="Disease", ylab="Score",
    col=colist, what=c(1,1,1,0))
  dev.off()
}

png(file=paste("PCA/Scores/score1 (scaled) all clusters.png", sep=''), 
  pointsize=16, width=5*512, height=3.5*512)
par(mfrow=c(6,1))
par(mar=c(2.5, 4, 1.5, 2))
for (p in 1:reaper1a@nGroups) {
  beanplot(score1.std.mat[,p] ~ mergedat2$type, xlab="Disease", ylab="Score",
    col=colist, what=c(1,1,1,0))
}
dev.off()


### weighted "score" to get sensible idea on expression differences 
### across different tumors 

for (i in 1:reaper1a@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(reaper1a@fit$P)[fit1==i]))
}

# score with unequal weights for genes in each cluster

check3.vec <- rep(0, reaper1a@nGroups)
check3.nosig.list <- list()
score3.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score3.std.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score3.abs.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score3.std.abs.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
comp3.list <- list()
num.groups.vec <- rep(0, reaper1a@nGroups)
for (i in 1:reaper1a@nGroups) {
  clustinm <- sort(rownames(reaper1a@fit$P)[fit1 == i])
  rnadatai <- mergedat[, clustinm]
  threshi <- Thresher(rnadatai, method="auer.gervini", scale=TRUE, agfun=agDimTwiceMean)
  check3.vec[i] <- threshi@pcdim
  check3.nosig.list[[i]] <- colnames(rnadatai)[threshi@delta<=0.3]
  reaperi <- Reaper(threshi, useLoadings=TRUE, method="auer.gervini")
  num.groups.vec[i] <- reaperi@nGroups
  compimat <- reaperi@spca@components[, 1]
  comp3.list[[i]] <- compimat
  comp3.good.gene <- setdiff(clustinm, check3.nosig.list[[i]])
  score3.mat[, i] <- reaperi@data[, comp3.good.gene] %*% compimat  # unscaled data (unsigned weights, could be negative)
  score3.std.mat[, i] <- scale(reaperi@data[, comp3.good.gene]) %*% compimat  # scaled data
  score3.abs.mat[, i] <- reaperi@data[, comp3.good.gene] %*% abs(compimat) # unscaled data with positive weights
  score3.std.abs.mat[, i] <- scale(reaperi@data[, comp3.good.gene]) %*% abs(compimat) # scaled data
  # png(file=paste("PCA/Scores2/cluster", i, " reaper.png", sep=''), pointsize=18, 
  #   width=2*512, height=1.5*512)
  # plot(reaperi)
  png(file=paste("PCA/Scores2/cluster", i, " pcdim.png", sep=''), pointsize=16, 
    width=3*512, height=1.5*512)
  plot(AuerGervini(threshi@spca))
  abline(h=1, lty=2, col=2)
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA/Scores2/score3 (no scale) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(-score3.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA/Scores2/score3 (scaled) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(-score3.std.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA/Scores2/score3 (no scale, abs) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(score3.abs.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA/Scores2/score3 (scaled, abs) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(score3.std.abs.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

save.image("merge.Rda")


## Use derived scores to dichotomize and re-define binary vectors

dich.score1.mat <- score1.mat
for (i in 1:ncol(score1.mat)) {
  indi <- which(dich.score1.mat[, i] >= median(dich.score1.mat[, i]))
  dich.score1.mat[indi, i] <- 1
  dich.score1.mat[-indi, i] <- 0
}

head(dich.score1.mat)

for (k in 1:length(tumorlist)) {
  sdir <- paste("Dichotomization/", tumorlist[k], sep='')
  if (!file.exists(sdir)) dir.create(sdir)
}

for (k in 1:length(tumorlist)) {
  png(file=paste("Dichotomization/", tumorlist[k], "/", tumorlist[k], " Score 1 (7 clusters).png", sep=''), 
    pointsize=18, width=3*512, height=1.5*512)
  par(mfrow=c(2, 3))
  par(mar=c(2.5, 4, 1.5, 2))
  for (l in 1:ncol(score1.mat)) {
    indk <- which(mergedat2$type==tumorlist[k])
    beanplot(score1.mat[indk, l] ~ mergedat2$type[indk], xlab="Disease", ylab=
      paste("Score 1: cluster", l, sep=' '), col=colist[[k]], what=c(0,1,0,0))
    abline(h=median(score1.mat[, l]), lty=2, col=2)
  }
  dev.off()
}

for (k in 1:length(tumorlist)) {
  png(file=paste("Dichotomization/", tumorlist[k], "/", tumorlist[k], " Score 1 (2D).png", sep=''), 
    pointsize=18, width=3*512, height=1.5*512)
  par(mfrow=c(3, 5))
  par(mar=c(4.5, 4, 1.5, 2))
  for (l1 in 1:(ncol(score1.mat)-1)) {
    for (l2 in (l1+1):ncol(score1.mat)) {
      indk <- which(mergedat2$type==tumorlist[k])
      plot(score1.mat[indk, l1], score1.mat[indk, l2], xlab=paste("S1: cluster", l1, sep=' '), 
        ylab=paste("S1: cluster", l2, sep=' '))
      abline(v=median(score1.mat[, l1]), lty=2, col=2)
      abline(h=median(score1.mat[, l2]), lty=2, col=2)
    }
  }
  dev.off()
}

for (k in 1:length(tumorlist)) {
  png(file=paste("Dichotomization/", tumorlist[k], "/", tumorlist[k], " Score 2 (7 clusters).png", sep=''), 
    pointsize=18, width=3*512, height=1.5*512)
  par(mfrow=c(2, 3))
  par(mar=c(2.5, 4, 1.5, 2))
  for (l in 1:ncol(score3.mat)) {
    indk <- which(mergedat2$type==tumorlist[k])
    beanplot(-score3.mat[indk, l] ~ mergedat2$type[indk], xlab="Disease", ylab=
      paste("Score 2: cluster", l, sep=' '), col=colist[[k]], what=c(0,1,0,0))
    abline(h=-median(score3.mat[, l]), lty=2, col=2)
  }
  dev.off()
}

for (k in 1:length(tumorlist)) {
  png(file=paste("Dichotomization/", tumorlist[k], "/", tumorlist[k], " Score 2 (2D).png", sep=''), 
    pointsize=18, width=3*512, height=1.5*512)
  par(mfrow=c(3, 5))
  par(mar=c(4.5, 4, 1.5, 2))
  for (l1 in 1:(ncol(score3.mat)-1)) {
    for (l2 in (l1+1):ncol(score3.mat)) {
      indk <- which(mergedat2$type==tumorlist[k])
      plot(-score3.mat[indk, l1], -score3.mat[indk, l2], xlab=paste("S2: cluster", l1, sep=' '), 
        ylab=paste("S2: cluster", l2, sep=' '))
      abline(v=-median(score3.mat[, l1]), lty=2, col=2)
      abline(h=-median(score3.mat[, l2]), lty=2, col=2)
    }
  }
  dev.off()
}


##############################################################################
# bean plots with sorted mean
library(Matrix)

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCAv/Scores/score1 (no scale) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=2*512)
  xidat <- data.frame(x=score1.mat[, p], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab="Score", 
    col=colisti, what=c(1,1,1,0))
  dev.off()
}

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCAv/Scores/score1 (scaled) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=2*512)
  xidat <- data.frame(x=score1.std.mat[, p], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab="Score", 
    col=colisti, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCAv/Scores2/score3 (no scale) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  xidat <- data.frame(x=-score3.mat[, i], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab=paste("Weighted PC ", 1, " Score", sep=''), 
    col=colisti, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCAv/Scores2/score3 (scaled) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  xidat <- data.frame(x=-score3.std.mat[, i], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab=paste("Weighted PC ", 1, " Score", sep=''), 
    col=colisti, what=c(1,1,1,0))
  dev.off()
}


save.image("merge.Rda")




##############################################################################
### apply Thresher for each type of tumor

for (k in 1:length(tumorlist)) {

sink(paste(outputDir, tumorlist[k], "/", tumorlist[k], ".txt", sep=''))

load(tumorda[k])

# Bartlett method
try(nBartlett(data.frame(scale(rnadata1t)), nrow(rnadata1t)),silent=FALSE)

# Broken-Stick method
spcak <- SamplePCA(t(scale(rnadata1t)))
lamk <- spcak@variances[1:(ncol(rnadata1t)-1)]
objk <- AuerGervini(spcak)
try(bsDimension(lamk),silent=FALSE)

# Randomization based method
rndLambdaF(scale(rnadata1t))

# Auer-Gervini method
compareAgDimMethods(objk, agfuns)

png(file=paste(outputDir, tumorlist[k], "/", tumorlist[k], ".png", sep=''), pointsize = 18,
    width=2*512, height=1.5*512)
plot(objk, agfuns)
dev.off()


## Use Thresher to remove outliers and cluster the features (default TwiceMean)

threshk <- Thresher(rnadata1t, method="auer.gervini", scale=TRUE, agfun=agDimTwiceMean)
threshk@pcdim
colnames(rnadata1t)[threshk@delta<=0.25]
reaperk <- Reaper(threshk, useLoadings=TRUE, cutoff=0.25, method="auer.gervini")
reaperk@nGroups
reaperk@bic

summary(reaperk@fit)
fitk <- apply(reaperk@fit$P, 1, which.max)
for (i in 1:reaperk@nGroups) {
  cat("Cluster", i, ":\n")
  print(sort(rownames(reaperk@fit$P)[fitk==i]))
}

clustk <- rbind(data.frame(RNA=colnames(rnadata1t)[threshk@delta<=0.3], cluster=rep(0,
  sum(threshk@delta<=0.3))), data.frame(RNA=rownames(reaperk@fit$P), cluster=as.numeric(fitk)))
clustk$RNA <- as.character(clustk$RNA)
clustk <- clustk[order(clustk[,1]), ]

write.table(clustk, paste(outputDir,tumorlist[k],"/",tumorlist[k], "_IL4_clusters(TwiceMean).csv", 
    sep=''), sep=",", col.names=NA, row.names=TRUE)

sink()

}


