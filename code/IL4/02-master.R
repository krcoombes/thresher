# make sure the directory for filled templates exists
source("00-paths.R")

filldir <- file.path(paths$scratch, "Filled")
if (!file.exists(filldir)) dir.create(filldir)

# make all the template files
system(paste("perl -w 01-fillTemplate.pl", filldir))

# get a list of the filled templates
templates <- dir(filldir, pattern="*.Rmd")

# make output directory
today <- file.path("..", "results", Sys.Date())
if (!file.exists(today)) dir.create(today)
html <- file.path(today, "HTML")
if (!file.exists(html)) dir.create(html)


library(rmarkdown)
for (temp in templates) {
  render(input = file.path(filldir, temp),
         knit_root_dir = getwd(),
         output_dir = html)
}
