---
title: Source Code
author: Min Wang and Kevin R. Coombes
---

# Source Code
This directory contains the source code to analyze the TCGA RNA
sequencing data on genes in the IL4 signaling pathway. Here are basic
instructions on the order in which scripts should be run.

1. Start with `00prepData.R`. This file reads the full TCGA
   RNA-Sequencing gene expression data files from the directory
   `../data/raw/TCGA-RNASeq`.  For each of 33 cancer types, it
   extracts the genes in the IL4 signaling pathway and stored the
   results in binary D data format int he directory
   `../data/clean/RDA`.
2. Do **not** directly execute either of the scripts whose file names
   begin with "01" (that is, `01-fillTemplate.pl` or
   `01-study33cancer.Rmd`).  The Rmarkdown file is a template; the
   perl script replaces the variables in the template to produce
   different versions of the Rmarkdown file, and stores those in the
   subdirectory `./Filled`. This process is overseen by the next
   script. 
3. Run the script `02-master.R`. This creates the `Filled`
   subdirectory, executes the perl script just mentioned, and then
   executes all of the specific filled-in versions of the Rmarkdown
   files. The results of those analyses are, in turn, stored by date
   in the folder `../results/[TODAY]`.

