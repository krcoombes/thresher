#########################################################################
### Get the full TCGA RNA sequencing data, then extract the part
### for genes in the IL4 pathway

source("00-paths.R")

## Here is where to get the full (sort-of-raw) data
rawDataDir <- paths$raw
tcgaDir <- file.path(rawDataDir, "TCGA-RNASeq")
if (!file.exists(tcgaDir)) stop("Cannot find tcga directory,", tcgaDir)

### Here is the list of tumor types. The exclusioans are for artififcal datasets
### that are the unioons of other sets.
allCancers <- dir(tcgaDir)
dontUse <- c("COADREAD", "GBMLGG", "KIPAN", "STES")
tumorlist <- allCancers[!(allCancers %in% dontUse)]
if (length(tumorlist) != 33) stop("Wrong number of tumor types.")
rm(dontUse)

### Here is where we will store the cleaned (in this case, subsetted) data
dataDir <- paths$clean
if (!file.exists(dataDir)) stop("Cannot find data directory,", dataDir)
rdaDir <- file.path(dataDir, "RDA")
if (!file.exists(rdaDir)) dir.create(rdaDir)

#################################################################################
## Load data in IL4 signaling pathway from TCGA RNA-seq cohort

## First, figure out which genes are actuall;y named in the pathway diagram.
library("XML")
sourceGPML <- file.path(dataDir, "IL4.gpml")
IL4genes <- NULL
gpmlTree <- xmlTreeParse(sourceGPML, useInternal=TRUE)
## loop over 'DataNode' items
ind1 <- as.numeric(which(names(xmlRoot(gpmlTree))=="DataNode"))
for (i in ind1) {
  ## get the gene ID from the 'TextLabel'
  genei <- xmlAttrs(xmlChildren(xmlRoot(gpmlTree))[[i]])[["TextLabel"]]
  genei <- gsub("[[:space:]]", "", genei)
  IL4genes <- c(IL4genes, genei)
} 

for (k in 1:length(tumorlist)) {
  fname <- file.path(rdaDir,
                     paste(tumorlist[k], "Rda", sep='.'))
  cat("working on", fname, "\n")
  if (file.exists(fname)) {
    load(fname)
  } else {
    ## read rna-seq data
    rawfile <- file.path(tcgaDir,
                         tumorlist[k],
                         paste(tumorlist[k], ".txt", sep=''))
    ## need to do this in two pieces because the second row just
    ## repeates "normlized counts" over and over. This is the part
    ## that is most likley to need changing if the data source fiddles
    ## with the format.
    ##
    ## KRC: The read.table command needs 'sep="\t"', else
    ##      you get the column names wrong.
    rnahead <- read.table(rawfile, header=TRUE, sep="\t",
                          nrow=1, row.names=1)
    rnadata <- read.table(rawfile, header=FALSE, sep="\t",
                          skip=2, row.names=1)
    colnames(rnadata) <- colnames(rnahead)
    rm(rnahead)
    ## figure out which genes are in the IL4 pathway
    Truegene <- unlist(lapply(rownames(rnadata), function(x)  {
      strsplit(x,'[|][[:space:]]*')[[1]][1]!="?" && 
        strsplit(x,'[|][[:space:]]*')[[1]][1] %in% IL4genes
    }))
    rnadata1 <- rnadata[Truegene,]
    ## Get the preferred genenames by splitting on vertical bar (|) ...
    genename1 <- unlist(lapply(rownames(rnadata1), function(x)  {
      strsplit(x,'[|][[:space:]]*')[[1]][1] 
    }))
    ## ... and use them
    rownames(rnadata1) <- genename1
    rnadata1 <- rnadata1[sort(genename1),]

    rnadata1t <- t(rnadata1)#[, -c(1,ncol(rnadata1))])

    save(IL4genes, genename1, rnadata1t, file=fname)
    rm(genename1, rnadata1t, fname,
       Truegene, rnadata, rnadata1, rawfile)
    gc()
  } # end else
} # end for k in 1:length(tumorlist)
rm(k)

save(allCancers, tumorlist, dataDir, rawDataDir, rdaDir, tcgaDir,
     file = file.path(paths$scratch, "globals.Rda"))
