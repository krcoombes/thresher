setClass("sim2wayParameters",
         representation=list(
           nSample = "integer",
           nProtein = "integer",
           nSampleClass = "integer",
           nProteinClass = "integer",
           protNull = "numeric",
           sampNull = "numeric",
           minSD = "numeric",
           maxSD = "numeric",
           meanSpread = "numeric"))

setMethod("summary", "sim2wayParameters", function(object, ...) {
  cat("\nNumber of sample rows:", object@nSample,
      "\nNumber of protein columns:", object@nProtein,
      "\nNumber of sample classes:", object@nSampleClass,
      "\nNumber of protein classes:", object@nProteinClass,
      "\nFraction of uninformative samples:", object@sampNull,
      "\nFraction of uninformative proteins:", object@protNull,
      "\nMinimum block SD:", object@minSD,
      "\nMaximum block SD:", object@maxSD,
      "\nMaximum block mean:", object@meanSpread,
      "\n\n")
})

sim2wayParameters <- function(nSample = 300,
                              nProtein = 100,
                              nSampleClass = 7,
                              nProteinClass = 10,
                              protNull = 0.50,
                              sampNull = 0.20,
                              minSD = 0.6,
                              maxSD = 0.8,
                              meanSpread = 0.8) {
  new("sim2wayParameters",
      nSample = as.integer(nSample),
      nProtein = as.integer(nProtein),
      nSampleClass = as.integer(nSampleClass),
      nProteinClass = as.integer(nProteinClass),
      protNull = protNull,
      sampNull = sampNull,
      minSD = minSD,
      maxSD = maxSD,
      meanSpread = meanSpread)
}

setClass("sim2way",
         representation=list(
           data = "matrix",
           params = "sim2wayParameters",
           sampleClass = "numeric",
           proteinClass = "numeric",
           blockSD = "matrix",
           blockMean = "matrix"))

sim2way <- function(params) {
  if (!inherits(params, "sim2wayParameters")) {
    stop("Invalid parameter class")
  }
  # classify proteins
  n <- params@nProteinClass-1
  protClass <- sample(1:params@nProteinClass, params@nProtein, replace=TRUE,
                      prob=c(params@protNull, rep((1-params@protNull)/n, n)))
  # classify samples
  n <- params@nSampleClass-1
  sampClass <- sample(1:params@nSampleClass, params@nSample, replace=TRUE,
                      prob=c(params@sampNull, rep((1-params@sampNull)/n, n)))
  # generate block standard deviations
  basesd <- matrix(runif(params@nSampleClass*params@nProteinClass,
                         params@minSD,
                         params@maxSD), 
                   nrow=params@nSampleClass,
                   ncol=params@nProteinClass)
  basesd[1,] <- 1
  basesd[,1] <- 1
  # generate block means
  basemean <- matrix(sample(seq(-params@meanSpread,
                                params@meanSpread,
                                length=5),
                            params@nProteinClass*params@nSampleClass,
                            replace=TRUE), 
                     ncol=params@nProteinClass,
                     nrow=params@nSampleClass)
  basemean[1,] <- 0
  basemean[,1] <- 0
  # generate the actual simulated data matrix
  data <- matrix(NA, ncol=params@nProtein, nrow=params@nSample)
  for (i in 1:params@nSample) {
    sc <- sampClass[i]
    data[i,] <- rnorm(params@nProtein,
                      basemean[sc, protClass],
                      basesd[sc, protClass])
  }
  # create the return value
  new("sim2way",
      data = data,
      params = params,
      sampleClass = sampClass,
      proteinClass = protClass,
      blockSD = basesd,
      blockMean = basemean)
}


setMethod("summary", "sim2way", function(object, short=TRUE, ...) {
  cat("Created using parameters:\n")
  summary(object@params)
  cat("\nDistribution of sample classes\n")
  print(table(object@sampleClass))
  cat("\nDistribution of protein classes\n")
  print(table(object@proteinClass))
  cat("\nDistribution of simulated data:\n")
  if (short) {
    summary(as.vector(object@data))
  } else {
    summary(object@data)
  }
})

if (FALSE) {
  library(ClassDiscovery)
  parset <-  sim2wayParameters() # use default values
  simmed <- sim2way(parset)

  simmed <- sim2way(sim2wayParameters(nSampleClass=3,
                                      nProteinClass=10,
                                      meanSpread=5,
                                      minSD=1, maxSD=1))
  nsc <- simmed@params@nSampleClass
  npc <- simmed@params@nProteinClass
  pcol <- hcl(seq(0, 360, length=npc+1)[1:npc], c=85, l=45)
  scol <- hcl(seq(0, 360, length=nsc+1)[1:nsc], c=95, l=65)
  metric <- "pearson"
  linkage <- "ward"
  mose <- Mosaic(simmed@data,
                 sampleMetric = metric, sampleLinkage = linkage,
                 geneMetric = metric, geneLinkage = linkage,
                 name = "Two-way simulation")
  plot(mose, col=redgreen(64), scale='row',
       sampleClasses=simmed@proteinClass, sampleColors=pcol,
       geneClasses=simmed@sampleClass, geneColors=scol)
}
