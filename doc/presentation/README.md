---
title: Documentation: Talks
author: Kevin R. Coombes
date: 2017-11-01
---

# Overview

This folder contains material used to create presentations about the
Thresher project. Ideally, each separate presentation gets a separate
"tag" in the version control repository allowing its source code to be
accessed.
