\documentclass[t]{beamer}
\usepackage{multicol}
\usepackage{sansmathaccent}
\pdfmapfile{+sansmathaccent.map}
\definecolor{krc}{rgb}{0.521, 0.118, 0.369} % 1C    3D   {0.3, 0.3, 0.9}
\definecolor{osuorange}{rgb}{0.8235,0.3725,0.0824}
\definecolor{osupurple}{rgb}{0.65,0.05,0.55}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up the meta-information for the PDF file
\hypersetup{
  pdftitle={Modeling Expression With Transcription Factors},
  pdfsubject={transcription factors},
  pdfauthor={Kevin R.~Coombes,
    Department of Biomedical Informatics,
    The Ohio State University,
    <coombes.3@osu.edu>
  },
  pdfkeywords={Thresher,hierarchical clustering,principal components analysis,outliers},
  pdfpagemode={None},
  pdfpagetransition=Replace,
  colorlinks,
  linkcolor=,%internal, deliberately left blank
  urlcolor=krc%external
  }

\usetheme[secheader]{Columbus}
% color the Sinput and Soutput chunks
\setbeamerfont{block body}{size=\small}
\usepackage[nogin]{Sweave}
\DefineVerbatimEnvironment{Sinput}{Verbatim} {xleftmargin=1em}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{xleftmargin=1em}
\DefineVerbatimEnvironment{Scode}{Verbatim}{xleftmargin=1em}
\fvset{listparameters={\setlength{\topsep}{0pt}}}
\newenvironment<>{Schunk}{%
  \begin{actionenv}#1%
    \parskip=0pt\vspace{\topsep}
    \def\insertblocktitle{}%
    \par\vspace{\topsep}%
    \usebeamertemplate{block begin}}
  {\par\parskip=0pt\vspace{\topsep}%
    \usebeamertemplate{block end}%
  \end{actionenv}}

\mode
<presentation>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%\setkeys{Gin}{width=\textwidth}

%%%%%%%%%%%%%%%%%%%%
% need the usual title, author, etc for splash page as well as title page
\title[Expression And Transcription Factors]{Modeling Expression With Transcription Factors} 
\author[\copyright\ Copyright 2017-18, Kevin R.~Coombes]{Kevin R. Coombes\\
  \textcolor{pms7532}{\tt coombes.3@osu.edu}
}
\institute{Department of Biomedical Informatics\\
  The Ohio State University
}
\titlegraphic{
  \includegraphics[width=3in]{./Figures/OSU-WexMedCtr-4C-Horiz-CMYK}
}
\date{22 January 2018}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% actually start creating frames
\begin{frame}[plain,noframenumbering]
  \titlepage
\end{frame}

\parskip=5pt plus3pt minus2pt  % probably belongs in a 'theme' style somewhere.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\begin{frame}
\frametitle{Motivation}
\textcolor{osuscarlet}{Question:} How much of (mRNA) gene expression
can be explained by the (RNA) expression of transcription factors?

\textcolor{osuscarlet}{Idea:} Use the pan-cancer RNA-Sequencing data
from The Cancer Genome Atlas (TCGA) to try to answer this question.

Note that the full data set consists of gene expression measurement on
more than 10,000 samples.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Transcription Factors}
\begin{frame}
\frametitle{Transcription Factors}

\textcolor{osuscarlet}{Get a list of transcription factors:}

\begin{itemize}
\item Go to the \textbf{TFCat Transcription Factor Database}${}^*$
  (\url{http://www.tfcat.ca/}).
\item Search for ``TF Gene'' and downloaded everything.
\item Database uses \textbf{mouse} Entrez Gene IDs as their
  primary identifier.
\item Convert gene symbols to upper case and look up the
  corresponding gene in humans.
\item Only retain genes that were manually curated with
  ``\textbf{strong}'' evidence of being transcription factors.
\item Final list contains 486 transcription factors.
\end{itemize}

{\small

\textcolor{osuorange}{${}^*$Fulton DL, Sundararajan S, Badis G, Hughes
  TR, Wasserman WW, Roach JC, Sladek R. TFCat: the curated catalog of
  mouse and human transcription factors. Genome Biology 2009; 10:R29.}

}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{TCGA Data}
\begin{frame}
\frametitle{TCGA RNA-Seq Data}

\textcolor{osuscarlet}{Get the data:}

\begin{itemize}
\item Download ``RSEM\_genes\_normalized'' TCGA RNA-sequencing data
  from  \url{http://firebrowse.org}
\item 33 cancer types
\item 10,446 samples
  \begin{itemize}
  \item 9,326 primary tumor
  \item 395 metastases
  \item 725 adjacent normal
  \end{itemize}
\item Number of samples per cancer type ranges between
  \begin{itemize}
  \item 45 (CHOL; cholangiocarcinoma)
  \item 1212 (BRCA; breast invasive carcinoma).
  \end{itemize}
\end{itemize}

\textcolor{osuscarlet}{Next step:} Perform Principal Components
Analysis (PCA) to get an overview of the data.

\end{frame}

\newcommand{\krc}{\vskip0pt plus 1filll}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{TCGA RNA-Seq Data}

\smallskip

\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/unnamed-chunk-3-1}
\end{center}

\krc
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Color Schemes}

Reference: \textcolor{osuorange}{Coombes KR, Brock G, Abrams ZB,
  Abruzzo LV, Polychrome: Creating and Assessing Qualitative Palettes
  With Many Colors. \textit{J Statist Software}. To appear.} Package
is available on CRAN.

\begin{center}
  \includegraphics[width=64mm]{../../results/legend}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Color Schemes}

\begin{center}
  \includegraphics[width=100mm]{../../results/knitfigs/scheme-1}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{TCGA RNA-Seq Data}

\begin{center}
  \includegraphics[width=100mm]{../../results/knitfigs/pca1_2-1}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{PCDimension}
\begin{frame}
\frametitle{Number of Principal Components}
\textcolor{osuscarlet}{Question:} How many (significant) principal
components are there?

\begin{itemize}
\item Bartlett's Method (in \texttt{nFactors} package): 485 (useless)
\item Broken Stick Method: 15 (conservative?)
\end{itemize}

\begin{center}
  \includegraphics[width=80mm]{../../results/knitfigs/plotBS-1}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{N Components}
\begin{frame}
\frametitle{Number of Significant Components}
Reference: \textcolor{osuorange}{Auer P, Gervini D. Choosing
principal components: a new graphical method based on Bayesian model
selection. Commun Stat Simulat, 2008; 37: 962--977.}

View the problem as one of model selection, where the model $M_D$ says
that there are $D$ components. Mathematically, says that eigenvalues
in the PC matrix satisfy
$$ \lambda_1 \ge \lambda_2 \ge \cdots \ge \lambda_D,
 \qquad \lambda_D > \lambda_{D+1},
 \qquad \lambda_{D+1} = \lambda_{D+2} = \cdots = \lambda_M $$

After specifying a prior distribution on $D$, they compute the
posterior probability of each model, and choose the model with the
maximum posterior probability.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Auer-Gervini Priors}
Use a family of prior distributions on the number $D$ of components.
$$ P(D) \propto exp(-\theta n D/ 2) $$
As $\theta \mapsto 0$, prior becomes flat: choose maximum possible
number of components. As $\theta \mapsto \infty$, prior increasingly
forces choice of zero components.

\begin{center}
  \includegraphics[width=106mm]{./Figures/ag-priors}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Evaluating All the Priors}
\begin{multicols}{2}
  \includegraphics[width=53mm]{./Figures/ag32}

Auer-Gervini plot the number $D$ of components as a function of the
prior parameter $\theta$.  This is a step function.  Longer steps mean
 more people would choose that value of $D$.
 \textcolor{osuscarlet}{Pick the ``highest step that is a reasonable length''.}

Our additions: [1] \textcolor{osuscarlet}{Define largest sensible
  $\theta$ as one that assigns $99\%$ probability to $D=0$.}  [2]
\textcolor{osuscarlet}{Test different definitions of ``reasonable''.}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{An Objective ``Reasonable'' Step Length}

Reference: \textcolor{osuorange}{Wang M, Kornblau SM, Coombes
  KR. Decomposing the Apoptosis Pathway Into Biologically
  Interpretable Principal Components. \textit{Cancer
    Informatics}. To appear.}

\texttt{PCDimension} package available on CRAN.

\begin{itemize}
\item Idea is to separate the step lengths into two groups: ``small''
  and ``significantly large''.
\item Developed an R package that implemented and tested eight
  criteria for separation:
    \begin{enumerate}   \itemsep2pt
    \item[\textcolor{black}{1--4:}] TwiceMean, Kmeans, Kmeans3 and Spectral.
    \item[\textcolor{black}{5--8:}] Ttest, Ttest2, CPT and CpmFun.
    \end{enumerate}
\item Compared with ``best'' methods from earlier studies.
\item Performed simulations with various data sizes and correlation
  structures.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Simulation Results}
\begin{center}
\includegraphics[height=60mm]{./Figures/fig2(lwd=3_error_0).jpeg} 
\end{center}
\textcolor{osuscarlet}{Auer-Gervini} (\textcolor{pink}{CPT} or
\textcolor{magenta}{TwiceMean}) and 
\textcolor{green}{rnd-Lambda} are the best on average.  Auer-Gervini
is two orders of magnitude faster.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Transcription Factors}
\subsection{Auer-Gervini}
\begin{frame}
\frametitle{Auer-Gervini Plot for Transcription Factors}
\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/ag-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{TF PCs}
\begin{frame}
\frametitle{Principal Components 3 and 4 (AML)}
\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/moreplots-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Principal Component 9 and 10 (Melanoma)}
\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/moreplots-4}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Principal Component 11 and 12 (PCPG; TGCT)}
\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/moreplots-5}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Principal Component 17 and 18 (Thymoma; Sarcoma)}
\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/moreplots-8}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Principal Component 27 and 28 (KICH; ACC)}
\begin{center}
  \includegraphics[width=102mm]{../../results/knitfigs/moreplots-13}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Non-linearity: Stochastic Neighbor Embedding}
\begin{center}
  \includegraphics[width=76mm]{../../results/knit10figs/cancers-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Thresher}
\begin{frame}
\frametitle{Thresher}

Reference: \textcolor{osuorange}{Wang M, Abrams ZB, Kornblau SM,
  Coombes KR. Thresher: Determining the Number of Clusters while
  Removing Outliers. \textit{BMC Bioinformatics}. 2018; 19(1):9.}

We built another R package, \texttt{Thresher}, that combines
Auer-Gervini to determine the PC dimension, outlier removal, and
feature clustering using mixtures of von Mises-Fisher distributions.

The package is available on CRAN.

\textcolor{osuscarlet}{Idea:} View the genes you want to cluster as
``features'' that produce ``weight vectors'' based on their
contributions to significant components. Outliers have short
length. Then cluster everything that remains based on their directions
in PC space.

\textcolor{blue}{Real Motivation: Sacrifices orthogonality for
a chance at getting biologically interpretable clusters.}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clustering on Hyperspheres}
\begin{frame}
\frametitle{Clustering on Hyperspheres}
\begin{center}
\includegraphics[width=80mm]{./Figures/vmf.png}
\end{center}

{\small\centering

Figure 1, Hornik and Gr\"un.

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Clustering on Hyperspheres}
I'll spare you the details.  Key points are [1] Someone has already
figured out which distribution to use:

Reference: \textcolor{osuorange}{Banerjee A, Dhillon IS, Ghosh J, Sra
  S. Clustering on the unit hypersphere using von Mises-Fisher
  distributions. J Mach Learn Res, 2005; 6: 1345--1382.}

and [2] someone else has already implemented this mixture model in an
R package:

Reference: \textcolor{osuorange}{Hornik K, Gr\"un B. movMF: An R
  package for fitting mixtures of von Mises-Fisher distributions. J
  Stat Software, 2014; 58(10): 1--31.}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Simulation Results}
\begin{frame}
\frametitle{Accuracy in Estimating the Number of Clusters}
\begin{center}
  \includegraphics[width=104mm]{Figures/fig2}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results for Transcription Factors}
\begin{frame}
\frametitle{Results for Transcription Factors}
\begin{itemize}
\item We applied \textbf{Thresher} to the TCGA transcription factor
  data set.
\item Found 30 clusters, which we are currently calling ``biological
  components''.
\item Looked at each transcription factor in UniGene to get a list of
  tissues where it is known to be expressed.
\item Recorded the ``most commonly seen tissue'' for the transcription
  factores in each cluster.
\item Also averaged the expression of transcription factors in each
  cluster to get ``biological scores'' for each sample.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{UniGene}
\begin{frame}
  \frametitle{UniGene}
\begin{center}
  \includegraphics[width=102mm]{Figures/SOX3-unigene}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{UniGene}
\begin{center}
  \includegraphics[width=102mm]{Figures/SOX3-profile}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{ToppGene}
We also computed Pearson correlation coefficients between each fo the
30 biological components and all 20,289 genes.

We slected all genes whose absolute correlation was greater than $0.5$
and uploaded them to ToppGene (\url{https://toppgene.cchmc.org/}) to
try to determine the associated fuinctional categories.

We noticed that \textcolor{osuscarlet}{some} of the gene sets (and
thus biological components) were associated with embryonically lethal
mouse phenotypes.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Biological Components}
\begin{frame}
\frametitle{Biological Component 4: Brain}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC4}
\end{center}

Gene List:
\texttt{
ASCL1
GSX1
MYT1
MYT1L
NEUROD1
NEUROD6
OLIG2
POU3F4
SOX1
SOX3
TBR1
}

UniGene Tissues: \texttt{brain}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 2: Liver}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC2}
\end{center}

Gene List:
\texttt{
ATF5
CEBPA
CREB3L3
ESRRA
GATA4
HNF1A
NR1H3
NR1H4
NR1I2
NR1I3
NR2F6
NR5A2
PCBD1
PPARA
PREB
TLE1
TLX1
ZFPM1
}

UniGene Tissues: \texttt{brain, intestine, liver}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 9: PCPG}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC9}
\end{center}

Gene List:
\texttt{
EGR4
FEV
HAND1
HAND2
NPAS4
PHOX2A
PHOX2B
PTF1A
TBX20
}

UniGene Tissues: \texttt{brain}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 17: Melanocytes}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC17}
\end{center}

Gene List:
\texttt{
ALX1
ETV5
FOXD3
PAX3
SOX10
}

UniGene Tissues: \texttt{embryonic tissue}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 25: Immune System}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC25}
\end{center}

Gene List:
\texttt{
CEBPE
CIITA
EOMES
GFI1
IKZF1
IKZF3
IRF8
PAX5
POU2AF1
POU2F2
SPI1
}

UniGene Tissues: \texttt{blood, brain, lung, lymph node}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 26: Intestine}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC26}
\end{center}

Gene List:
\texttt{
CDX1
CDX2
NEUROG3
PAX4
PDX1
}

UniGene Tissues: \texttt{intestine}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 20: TCGT (Lethal)}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC20}
\end{center}

Gene List:
\texttt{
AIRE
FOXH1
GCM2
HES3
HOXB1
NANOG
NR5A1
SPZ1
SRY
T
UTF1
}

UniGene Tissues: \texttt{embryonic tissue, testis}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 1: Cell Cycle (Lethal)}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC1}
\end{center}

Gene List:
\texttt{
CEBPZ
GMEB1
KDM5B
NCOA3
NFYA
NR6A1
POLR2B
SKIL
TAF1A
TAF2
TCF20
TDG
TFAM
TFDP1
UBP1
YY1
ZBTB33
}

UniGene Tissues: more than four
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Component 29: Angiogenesis (Lethal)}
\begin{center}
  \includegraphics[width=120mm]{../../results/ManyFigs/sorted-BC29}
\end{center}

Gene List:
\texttt{
ARNTL   BCL6    EPAS1   FOXC1   FOXO1   FOXP2   GLI2    GTF2A1L HIF1A   MAFB   
MEIS2   MEOX2   NFE2L1  NR1D1   NRG1    PPARD   RXRA    SOX9    STAT3   TCF7L1 
TEAD1   TFE3    WWTR1
}

UniGene Tissues: more than four
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modeling Expression}
\begin{frame}
\frametitle{Modeling Expression}

Now we can retun to our starting question: \textcolor{osuscarlet}{To
  what extent does the mRNA expression of transcription factors
  explain the mRNA expression of other genes?}

We obviously won't use all 486 transcription factors.  Instead, we'll
use the 30 biological components that we have derived from our
clustering analysis.  So, for each gene, we want to fit a model that
looks like:

\begin{block}{}
  \begin{center}
    Y $\sim$ BC1 + BC2 + BC3 + $\dots$ + BC30.
  \end{center}
\end{block}

We fit the model with half the data and used it to make predictions
on the other half.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{P Values}
\begin{frame}
\frametitle{Why P-Values Are Worthless}

$99.8\%$ of the p-values are less than $0.01$. (There are 5000 samples.) 
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/bum-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Studentized Residuals}
\begin{frame}
\frametitle{Studentized Residuals}
We looked at the studentized residuals on the test data. Most are
pretty small.
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/student-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Counting Outliers}
We counted the number of ``outlier samples'' per gene (defined as residuals
that are 4$\sigma$ away from the mean).
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/outliers-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tests of Normality}
\begin{frame}
\frametitle{Kolmogorov-Smirnov Test of Normality}
We computed Kolmnogorov-Smirov statistics for the studentized residuals
on the test data. Of course, nothing is normal.
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/KS-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Cr\'amer - von Mises Test of Normality}
Similar results for the Cr\'amer  - von Mises statistics.
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/CVM-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Jarque-Bera Test of Normality}
And for Jarque-Bera.
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/logJB-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Newman Test of Normality}
And for the newman studentized range statistic.
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/newman-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{R-squared}
\begin{frame}
\frametitle{R-squared values}
Finally, we looked at the $R^2$ values, which we can think of as the
percentage of variance explained by the model.
\begin{center}
  \includegraphics[width=96mm]{../../results/knit3figs/R2-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Comparing Metrics}
\begin{frame}
\frametitle{Comparing Metrics}
\begin{center}
  \includegraphics[width=72mm]{../../results/knit3figs/pairs-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{After Removing Genes With Zero Counts in Some Samples}
\begin{center}
  \includegraphics[width=72mm]{../../results/knit3figs/pairs2-1}
\end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
\item \texttt{Polychrome} produces useful palettes with lots of colors.
\item \texttt{PCDimension} (using the Twice mean criterion to automate
  the Auer-Gervini algorithm) seems like the best method to determine
  the number of principal components.
\item \texttt{Thresher} produces biologically interpretable
  non-principal components (BCs).
\item A combination of UniGene and ToppGene provides sensible
  interpretations of the BCs derived from the transcription factors.
\item We still need a good metric to determine the quality of a linear
  model when we train and test it on 5000 samples.
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Acknowledgement}
 \begin{itemize} 
  \item Dr. Steven Kornblau, The University of Texas MD Anderson Cancer
Center.
  \item Dr. Min Wang, The Ohio State University.
  \item Dr. Zachary Abrams, The Ohio State University.
  \item Mark Zucker, The Ohio State University.
  \item Kristi Bushman, University of Pittsburgh.
  \item[]
  \item Grants: NSF No. 1440386; NIH/NCI P30 CA016058; R01 CA182905,
    NIH/NLM T15LM011270.
 \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOC at end ...
\begin{frame}[plain,noframenumbering]
\begin{multicols}{2}
  \tableofcontents
\end{multicols}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
