---
author: Min Wang, Zachary B. Abrams, Kristi Bushmill, and Kevin R. Coombes
title: IL4 Thresher Manuscript
---

This folder contains the manuscript to go along with our application of
Thresher to the IL4 Signaling Pathway. This is written in LaTeX, with
the references perpared using BibTeX.

