\section*{Discussion}\label{discussion}

\subsection*{Expression of transcription factors separates cancer types}\label{t-sne-map}

We began by testing the hypothesis that transcription factor expression 
could differentiate cancers in the TCGA dataset. The results displayed in the 
nonlinear t-SNE map (\fref{sne}) clearly demonstrate that using 30 biological 
components derived from 486 transcription factors produced a clear separation 
between most TCGA cancer types. This map illustrates the relative separation 
or biological distance between cancer types based on 
transcription factor expression. This visualization displays a more explicit
separation between cancer types than any of the principal component 
plots alone, thus producing important biological insights not observable through
simpler linear methods.

Our initial observation is that cancer types that occur in the same or
similar tissues or organ systems may be difficult to distinguish. Both
low-grade gliomas (LGG) and glioblastomas (GBM), for example, occur in
the brain. These two diseases are plotted near each other in
\fref{sne}; in fact, they overlap slightly. Moreover, the
transcription factor clustering groups them closer to each other than
to any other cancer. This grouping is understandable given that some
of the biological components are specific for transcription factors
expressed in the brain. Other examples include rectal adenocarcinoma
entirely overlapping with colon adenocarcinomas, both uterine cancers
clustering together, and some esophageal cancers overlapping stomach
cancers.

The examples from the previous paragraph might lead one to suspect
that the separation we are seeing is driven not by cancer type but by
baseline differences in TF expression in the tissues where the cancers
originate. However, there is evidence from other cancer types that
tissue type alone does not completely explain the results.  For
example, TCGA studied three different types of kidney cancer, and
there are four associated clusters in the t-SNE map. Two of these
clusters appear next to each other at the right center of the map;
they represent kidney renal clear cell carcinoma (KIRC) and kidney
renal papillary cell carcinoma (KIRP).  The other two clusters also
appear next to each other, but in the middle of the bottom portion of
the map. One of these contains samples of normal kidney coming from
all three studies. The final cluster contains all of the kidney
chromophobe (KICH) cases, along with a few KIRC and KIRP cases. The
relative positions of the three types of kidney cancer are consistent
with recent reports that KIRC and KIRP samples are similar to proximal
tubule segments, whereas KICH samples are more similar to distal
segments \cite{davis2014,lee2015,chen2016}.

Samples derived from lung tissue display a similar phenomenon. TCGA
studied both lung adenocarcinoma (LUAD) and lung squamous cell
carcinoma (LUSC) histological subtypes.  Our results find three
clusters of lung-derived samples that represent (in order, lying on a
ray emanating from the center of the figure) LUSC, LUAD, and normal
lung.  In particular, (1)~normal samples cluster together, (2)~normal
samples are separate from either cancer group, and (3)~the squamous
cell and adenocarcinomas are clearly distinct. It also suggests that
transcription factor expression in normal lung tissue may be more
similar to lung adenocarcinoma than to lung squamous cell carcinoma.
These findings are consistent with the fact that the two histologies
of lung cancer arise from different cell types. LUSC arises from the
 squamous epithelium that lines the airways and alveoli, while
LUAD arises from the more numerous glandular or alveolar type 2 cells
\cite{mainardi2014,sutherland2014,li2015}. 

The distinction between squamous cell carcinomas and adenocarcinomas
is present throughout \fref{sne}.  Adenocarcinomas (including prostate
(PRAD), colon (COAD), lung (LUAD), pancreas (PAAD), ovarian (OV),
stomach (STAD), and some esophagus (ESCA) tumors) appear to be
scattered around the periphery of the map.  By contrast, squamous cell
carcinomas (including lung (LUSC), cervix (CESC), head and neck
(HNSC), and esophagus (ESCA)) cluster near each other, regardless of
the organ system, in the center of the map. This observation suggests
an underlying similarity in the transcription factor expression
profiles of the squamous cell cancers regardless of the tissue type of
squamous cell cancer.

Breast cancer (BRCA) illustrates a different phenomenon.  Most samples
are in one large cluster, with normal samples in a distinct small
separate cluster nearby. However, the triple negative cases form a
completely independent cluster separate from either the normal samples
or the main cluster of breast cancer samples. This indicates that
triple negative breast cancer, in terms of transcription factor
expression, represents a distinct and completely separate form of
breast cancer.  Using the transcription factor components that
separate these triple negative cases may prove useful in treating
triple negative breast cancer patients through a better understanding
of the underlying molecular biology.

In every cancer study where TCGA has included normal controls, the
t-SNE map shows that the normal samples differ from the tumors.  In
most cases, they form a completely separate cluster.  In others, like
prostate (PRAD), thyroid (THCA), or bladder (BLCA), they can be found
on the periphery of the tumor cluster. This differentiation shows that
transcription factor expression alone is able to differentiate cancer
from the adjacent normal tissue. This is of particular importance due to
its potential applications in translational medicine and potential use
in cancer screenings.

Other research groups have already applied t-SNE and related
methodologies to TCGA data in order to separate different types of
cancer \cite{falco2016,li2017,way2018,taskesen2016}.  Those studies
used the entire transcriptome of 20,000 genes, unlike our study that
restricts itself to only~486 transcription factors. In every case, our
findings using only TFs are similar to the results from these previous
studies. Significantly, the inability to (fully) separate certain
pairs of cancers, such as COAD/READ and UCS/UCEC, was seen previously
by researchers using the whole transcriptome \cite{li2017}. This
finding shows that our inability to separate those cancers does not
occur because we only used TFs. Overall, the consistency between our
results and previous whole transcriptome pan-cancer studies
strengthens the underlying hypothesis that transcription factors may
be the primary driver for the differentiation between different cancer
types in various tissue types.


\subsection*{Biological components}\label{biological-components}

We used Thresher to cluster transcription factors according to a
transformation of their expression into 30 one-dimensional biological
components. We then hypothesized that each biological component was
associated with a particular biological process. Examining the biology
underlying each of the 30 components revealed two general categories
of transcription factor clusters: 12 were tissue specific and 18 were biological
function specific. Among the 18 function-associated clusters, 8 were
also associated with embryonically lethal mouse phenotypes. The
tissue specific components consist of transcription factors produced
only within the cancers arising from that tissue type.  In
embryonic lethal components, the transcription factors were part of
universally expressed pathways such as the cell cycle. Examples of
tissue specific pathways are shown in \fref{beans.tissue}. It is clear
that certain cancers have a significantly higher expression of a
particular cluster of transcription factors relative to other
cancers. This makes biological sense, as biological processes peculiar
to a given tissue type would be expected to be specifically altered in
cancer specific to that tissue.

\fref{beans.lethal} further validates this pattern in the context of constitutive or 
embryonic lethal components. In these cases there is little, or no difference of 
expression between cancer types since the transcription factors that make up these
components are \emph{comparably} expressed across all tissue types, a requirement
for self-viability. Thus it is the tissue specific components, and especially
those that differentiate between normal and cancerous samples within a specific
cancer and those that differentiate between two cancers in the same organ system,
that are of particular clinical utility and interest as biomarkers.

Overall, these patterns demonstrate Thresher's effectiveness at
clustering genes by expression. The fact that transcription factor
clusters associated with biological processes necessary for viability
show similar expression levels across cancers is an important
validation. Additionally, our finding that differentiation between
transcription factor clusters tends to correspond to differentiation
of cases (whether they are cancer or normal samples), or by the type
or tissue of origin, as well as by biological process, indicates that
our method yields clustering patterns that correspond to real
underlying biological differences.

\subsection*{Conclusion}\label{conclusion}

Transcription factors play a vital role in regulating gene
expression. By applying the Thresher method, we were able to summarize
the activity of 486 transcription factors using only 30 distinct
biological components. Analyzing these components helps us better
understand how transcription factors interact with each other in
regulatory networks.  Moreover, the expression data summarized by this small set 
of biological components was sufficient to distinguish most of the different cancer 
types and to separate tumors from normal controls within cancer types. This suggests 
that patterns in these biological components may be useful in understanding the 
underlying biology of cancers. Additionally, since transcription factors are common 
targets for treatment, these patterns may also be useful for identifying
viable genes to target in new treatments or in developing treatment
regimens for various subtypes of cancer.

The methodology that combines Thresher with t-SNE maps should be
broadly applicable. It can, in principle, be used to understand the
regulatory control that microRNAs and methylation have on gene and
protein expression. It can also be applied to other biologically
meaningful subsets of genes than transcription factors; obvious
candidates for future study include sets of genes that are known to
interact in signaling pathways or in the regulation of mechanisms like
apoptosis. 
