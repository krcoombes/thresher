---
title: Documentation
author: Kevin R. Coombes
date: 2017-11-01
---

# Overview

This folder contains documentation for the project in which we apply
the Thresher algorithm to understand transcription factors.  Separate
directories are supplied for the manuscript (`paper`) and for any
talks given related to the project (`presentation`).
