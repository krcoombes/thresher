---
author: Keivn R. Coombes
title: Clustering and Transcription Factors
date: 2018-02-08
---

Various things this title might refer to:

+ Clustering transcription factor binding site motifs based on the
  sequences to find similar ones
    1. castro-mondragon2017
+ Clustering of transcription factor binding sites (for the same or
  different TFs) based on their location in the genome
	1. gotea2010
	1. iwasaki2013
	1. murakami2004
	1. wollmann2017
	1. yan2013
	1. zhang2006
+ Predicting complexes of transcription factors from their motifs
    1. han2015
+ Predicting TF pairs based on gene co-exporession patterns
    1. zeidler2016
+ Classifying transcription factors by the DNA binding domains in
  their protein products.
    1. ENCODE2012
	1. wingdender2015
	1. piatek2013

