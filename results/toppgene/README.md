---
Title: ToppGene Results
Author: Kevin R. Coombes
Date: 2017-11-17
---

# Results

Report `108-correl.Rmd` generated, for each transcription factor
cluster (or biological component), a list of genes whose Pearson
correlation with the average expression in that cluster was at least
0.05. Those gene lists were manually uploaded to the ToppGene web site
on 2017-11-17. Using the defauklt settings at the web site, we
performed gene set analysis. Results were manually downloaded and
saved to this folder. Brief summaries for each cluster were stored in
the file `00interpret.tsv`.
