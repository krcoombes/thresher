---
Title: Results
Author: Kevin R. Coombes
Date: 2017-10-23
---

# Results
This folder contains results produced by the analyses performed in the
`./code` directory.  In general, these are organized by the datae that
the analysis was performed.
